const HtmlWebPackPlugin = require("html-webpack-plugin");
const webpack = require("webpack")
const path = require("path")

const ENV = process.env.NODE_ENV || 'development';

const API_BASE_URL = {
  "development": "http://localhost:3000",
  "staging": "",
  "production": ""
}[ENV]

const FILES_API_BASE_URL = {
  "development": "http://localhost:3004",
  "staging": "",
  "production": ""
}[ENV]

var config = {
    output: {
      path: path.resolve(__dirname, 'dist'),
      publicPath: "/"
    },
    entry: "./src/index.js",
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        },
        {
          test: /\.html$/,
          use: [
            {
              loader: "html-loader"
            }
          ]
        },
        {
          test: /\.css$/,
          use: [ 'style-loader', 'css-loader' ]
        },
        {
          test: /\.(jpe?g|png|svg|gif)$/,
          use: {
            loader: "file-loader?name=./assets/images/[name].[ext]"
          }
        },
        {
          test: /\.mp3$/,
          use: {
            loader: 'file-loader?name=./assets/sounds/[name].[ext]'
          }
        },
        {
          test: /\.(eot|svg|ttf|woff|woff2)$/,
          use: ["file-loader"]
        }
      ]
    },
    resolve: {
      extensions: [".ts", ".tsx", ".js", ".json"]
    },
    devServer: {
      contentBase: path.join(__dirname, "dist"),
      compress: true,
      historyApiFallback: true,
      host: '0.0.0.0',
      port: 1234,
      disableHostCheck: true
    },
    devtool: "source-map",
    plugins: [
      new HtmlWebPackPlugin({
        template: "./src/index.html",
        filename: "./index.html",
        favicon: "./src/assets/favicon.ico"
      }),
      new webpack.DefinePlugin({
        BITBUCKET_BUILD_NUMBER: JSON.stringify(process.env.BITBUCKET_BUILD_NUMBER || 1),
        API_BASE_URL: JSON.stringify(API_BASE_URL),
        FILES_API_BASE_URL: JSON.stringify(FILES_API_BASE_URL),
        APPLICATION_KEY: JSON.stringify("5Syw6Vr4gmVDbxtGv5Q3kvBR2XxDCDrEMvpT49JVkFDVecJX")
      })
    ]
};

module.exports = config