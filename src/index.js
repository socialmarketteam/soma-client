import React from 'react';
import ReactDOM from 'react-dom';
import './assets/vendor/bootstrap/css/bootstrap.min.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import store from "./store"
import { Provider } from "react-redux"
import Firebase from "./firebase"
Firebase.init()

ReactDOM.render((
    <Provider store={store}>
        <App />
    </Provider>
), document.getElementById('root'));
registerServiceWorker();
