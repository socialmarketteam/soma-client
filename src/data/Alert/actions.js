export const Types = {
    ALERT_DID_PUSH: "ALERT_DID_PUSH",
    ALERT_DID_REMOVE: "ALERT_DID_REMOVE"
}

import uuid from "uuid/v4"

export default {
    pushAlert: (alert) => {
        return dispatch => {
            alert.uuid = alert.uuid || uuid()
            dispatch({
                type: Types.ALERT_DID_PUSH,
                data: alert
            })
        }
    },
    removeAlert: (alertUuid) => {
        return dispatch => {
            dispatch({
                type: Types.ALERT_DID_REMOVE,
                data: alertUuid
            })
        }
    }
}