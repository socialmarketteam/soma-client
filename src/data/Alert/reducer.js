import {Types} from "./actions"

const initialState = {
    alerts: []
}

export default (state = initialState, action) => {
    switch(action.type) {
        case Types.ALERT_DID_PUSH:
            return Object.assign({}, state, {
                alerts: action.data ? [...state.alerts, action.data] : state.alerts
            })
        case Types.ALERT_DID_REMOVE:
            return Object.assign({}, state, {
                alerts: action.data ? state.alerts.filter(alert => alert.uuid != action.data) : state.alerts
            })
        default: return state;
    }
}