import {createChatChanel} from "../../socket"
import uuid from "uuid/v4"
import Firebase from "../../firebase"
import SomaApi from "../../SomaApi";

export const Types = {
    CHAT_BOX_WILL_ADD: "CHAT_BOX_WILL_ADD",
    CHAT_BOX_DID_ADD: "CHAT_BOX_DID_ADD",
    CHAT_CONVERSATION_WILL_LOAD: "CHAT_CONVERSATION_WILL_LOAD",
    CHAT_CONVERSATION_DID_LOAD: "CHAT_CONVERSATION_DID_LOAD",
    CHAT_BOX_DID_CLOSE: "CHAT_BOX_DID_CLOSE",
    CHAT_MESSAGE_WILL_SEND: "CHAT_MESSAGE_WILL_SEND",
    CHAT_MESSAGE_DID_SEND: "CHAT_MESSAGE_DID_SEND"
}

var actions = {}

actions.popUpChatbox = (conversationId) => {
    if(!conversationId) return null;
    return dispatch => {
        Firebase.firestore.conversation.getConversation(conversationId, (docSnap) => {
            var data = docSnap.data();
            var {users, messages, name} = data;
            SomaApi.shared.request().data("GET", "user", "users", {
                filter: JSON.stringify({
                    _id: {$in: users}
                })
            }).then(response => {

                var conversation = {
                    users: response.data,
                    messages,
                    name,
                    uuid: uuid(),
                    isLoading: false
                }
                dispatch({
                    type: Types.CHAT_BOX_DID_ADD,
                    data: conversation
                })
            }).catch(error => {
                console.log(error)
            })
        })
    }
}

actions.subscribeToConversation = (conversation) => {
    return dispatch => {
        if(!conversation || !conversation.uuid || !conversation.id) return;
        const conversationUpdated = (conversationUuid) => {
            return (doc) => {
                dispatch(actions.conversationUpdated(conversationUuid, doc))
            }
        }
        Firebase.firestore.conversation.subscribeToConversation(conversation.id, conversationUpdated(conversation.uuid))
    }
}

actions.sendMessage = (conversationId, message) => {
    if(!conversationId || !message) return null;
    return dispatch => {
        dispatch({
            type: Types.CHAT_MESSAGE_WILL_SEND
        })

        Firebase.firestore.conversation.sendMessageToConversation(conversationId, message)
    }
}

actions.closeChatBox = (uuid) => {
    if(!uuid) return null;
    return dispatch => {
        dispatch({
            type: Types.CHAT_BOX_DID_CLOSE,
            uuid: uuid
        })
    }
}

actions.addChatBox = ({users = [], chatName = "", imagePath = ""}) => {
    if(!users || !users.length) return null;
    return dispatch => {
        dispatch({
            type: Types.CHAT_BOX_WILL_ADD
        })

        var conversation = {
            users,
            messages: [],
            name: chatName,
            uuid: uuid(),
            isLoading: false
        }

        dispatch({
            type: Types.CHAT_BOX_DID_ADD,
            data: conversation
        })
    }
}

actions.loadConversation = (conversation) => {
    if(!conversation || !conversation.uuid) return null;
    return dispatch => {
        dispatch({
            type: Types.CHAT_CONVERSATION_WILL_LOAD,
            uuid: conversation.uuid
        })
        
        createChatChanel(conversation.users.map(user => user._id), conversation.name, (error, createdDoc) => {
            dispatch({
                type: Types.CHAT_CONVERSATION_DID_LOAD,
                data: createdDoc,
                uuid: conversation.uuid,
                error
            })
        })
    }
}

actions.conversationUpdated = (conversationUuid, doc) => {
    if(!doc) return null;
    return dispatch => {
        dispatch({
            type: Types.CHAT_CONVERSATION_DID_LOAD,
            data: doc,
            uuid: conversationUuid
        })
    }
}

export default actions