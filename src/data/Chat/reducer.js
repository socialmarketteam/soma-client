import {Types} from "./actions"

const initState = {
    chatBoxes: [],
    isAdding: false
}

const chatReducer = (state = initState, action) => {
    if(!action) {
        return initState
    }

    switch(action.type) 
    {
        case Types.CHAT_CONVERSATION_WILL_LOAD:
            if(!action.uuid) return state;
            let loadingConversation = state.chatBoxes.find(box => box.uuid === action.uuid)
            if(!loadingConversation) return state;
            return Object.assign({}, state, {
                chatBoxes: state.chatBoxes.map(box => {
                    if(box.uuid !== action.uuid) return box;
                    return Object.assign({}, box, {
                        isLoading: true
                    })
                })
            })
        case Types.CHAT_CONVERSATION_DID_LOAD:
            if(!action.uuid) return state;
            let loadedConversation = state.chatBoxes.find(box => box.uuid === action.uuid)
            if(!loadedConversation) return state;
            return Object.assign({}, state, {
                chatBoxes: state.chatBoxes.map(box => {
                    if(box.uuid !== action.uuid) return box;
                    return Object.assign({}, box, {
                        isLoading: false,
                        messages: action.error ? box.messages : action.data.messages,
                        users: action.error || !action.data.users.filter(user => user._id && user.name).length ? box.users : action.data.users,
                        id: action.data ? action.data.id : undefined
                    })
                })
            })
        case Types.CHAT_BOX_DID_CLOSE:
            if(!action.uuid) return state;
            let existing = state.chatBoxes.find(box => box.uuid === action.uuid);
            if(!existing) return state;
            return Object.assign({}, state, {
                chatBoxes: state.chatBoxes.filter(box => box.uuid !== existing.uuid)
            })
        case Types.CHAT_BOX_WILL_ADD:
            return Object.assign({}, state, {
                isAdding: true
            })
        case Types.CHAT_BOX_DID_ADD:
            let existingBox = action.data && action.data.users ? state.chatBoxes.find(box => {
                return box.users.equals(action.data.users) || box.id === (action.data.id || null)
            }) : undefined
            return Object.assign({}, state, {
                isAdding: false,
                chatBoxes: action.data && !existingBox ? [...state.chatBoxes, action.data]
                : state.chatBoxes
            })
        case "INIT":
            return initState;
        default: 
            return state
    }
}

export default chatReducer