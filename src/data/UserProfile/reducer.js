import { Types } from "./actions"

const initState = {
    "me": null,
    "data": null,
    "network": {
        "errors": []
    },
    "isLoading": false,
    "isUpdating": false,
    "friendRequests": []
}

const userProfileReducer = (state = initState, action) => {
    switch (action.type) {
        case Types.USER_PROFILE_DID_UPDATE:
            return Object.assign({}, state, {
                isUpdating: false
            })
        case Types.USER_PROFILE_WILL_UPDATE:
            return Object.assign({}, state, {
                isUpdating: true
            })
        case Types.FRIEND_REQUEST_WILL_SEND:
            let existingRequest = state.friendRequests.find(request => request.fromId === action.fromId && request.toId === action.toId)
            return Object.assign({}, state, {
                friendRequests: existingRequest ? [...state.friendRequests.filter(request => request.fromId !== action.fromId && request.toId !== action.toId), Object.assign({}, existingRequest, {
                    isSending: true
                })]  : [{
                    fromId: action.fromId,
                    toId: action.toId,
                    isSending: true
                }]
            })
        case Types.FRIEND_REQUEST_DID_SEND:
            let awaitingRequest = state.friendRequests.find(request => request.fromId === action.fromId && request.toId === action.toId)
            return Object.assign({}, state, {
                friendRequests: awaitingRequest ? [...state.friendRequests.filter(request => request.fromId !== action.fromId && request.toId !== action.toId), Object.assign({}, awaitingRequest, {
                    isSending: false
                })] : [{
                    fromId: action.fromId,
                    toId: action.toId,
                    isSending: false
                }],
                network: Object.assign({}, state.network, {
                    errors: action.error ? [...state.network.errors, action.error] : state.network.errors
                })
            })
        case Types.USER_PROFILE_WILL_LOAD: 
            return Object.assign({}, state, {
                isLoading: true
            })
        case Types.USER_PROFILE_DID_LOAD:
            return Object.assign({}, state, {
                isLoading: false,
                data: action.data,
                error: action.error ? [...state.network.errors, action.error] : state.network.errors
            })
        case Types.USER_PROFILE_WILL_CREATE:
            return Object.assign({}, state, {
                "me": action.userProfile,
                "network": {
                    "errors": []
                }
            })
        case Types.USER_PROFILE_DID_CREATE:
            return Object.assign({}, state, {
                "me": action.userProfile ? action.userProfile : state.me,
                "network": {
                    "errors": action.error ? [...state.network.errors, action.error] : state.network.errors
                }
            })
        case Types.USER_PROFILE_CLEAR_ERROR:
            return Object.assign({}, state, {
                "network": {
                    "errors": []
                }
            })
        case "INIT":
            return initState;
        default:
            return state
    }
}

export default userProfileReducer