import SomaApi from "../../SomaApi"
import sessionActions from "../UserSession/actions"
import alertActions from "../Alert/actions"
import uuid from "uuid/v4"

export const Types = {
    USER_PROFILE_WILL_CREATE: "USER_PROFILE_WILL_CREATE",
    USER_PROFILE_DID_CREATE: "USER_PROFILE_DID_CREATE",
    USER_PROFILE_CLEAR_ERROR: "USER_PROFILE_CLEAR_ERROR",
    USER_PROFILE_WILL_LOAD: "USER_PROFILE_WILL_LOAD",
    USER_PROFILE_DID_LOAD: "USER_PROFILE_DID_LOAD",
    FRIEND_REQUEST_WILL_SEND: "FRIEND_REQUEST_WILL_SEND",
    FRIEND_REQUEST_DID_SEND: "FRIEND_REQUEST_DID_SEND",
    FRIEND_REQUESTS_WILL_LOAD: "FRIEND_REQUESTS_WILL_LOAD",
    FRIEND_REQUESTS_DID_LOAD: "FRIEND_REQUESTS_DID_LOAD",
    USER_PROFILE_WILL_UPDATE: "USER_PROFILE_WILL_UPDATE",
    USER_PROFILE_DID_UPDATE: "USER_PROFILE_DID_UPDATE"
}

let actions = {}

actions.updateProfile = (userProfile) => {
    return dispatch => {
        if(!userProfile || !userProfile._id) return;
        dispatch({
            type: Types.USER_PROFILE_WILL_UPDATE
        })

        SomaApi.shared.request().data("PATCH", "user", "users", undefined, userProfile, userProfile._id)
        .then(response => {
            dispatch({
                type: Types.USER_PROFILE_DID_UPDATE
            })
            dispatch(sessionActions.sessionUserWillReloadAction())
        })
        .catch(error => {
            dispatch({
                type: Types.USER_PROFILE_DID_UPDATE
            })
            dispatch(alertActions.pushAlert({
                uuid: uuid(),
                message: "Sorry! We cannot update your profile. Please try again later",
                type: "danger"
            }))
        })
    }
}

actions.unfriend = (userId) => {
    if(!userId) return null;
    return dispatch => {
        SomaApi.shared.delete(`/user/friends/${userId}`)
        .then(() => {
            dispatch(sessionActions.sessionUserWillReloadAction())
        })
        .catch(error => {

        })
    }
}

actions.acceptFriendRequest = (fromId) => {
    if(!fromId) return null;
    return dispatch => {
        SomaApi.shared.post(`/user/friend_request/accept`, {
            fromId
        })
        .then(() => {
            dispatch(sessionActions.sessionUserWillReloadAction())
        })
        .catch(error => {

        })
    }
}

actions.friendRequestWillCancelAction = (fromId, toId) => {
    if(!fromId || !toId) return null;
    return dispatch => {
        SomaApi.shared.delete('/user/friend_request', {
            fromId,
            toId
        })
        .then(response => {
            dispatch(sessionActions.sessionUserWillReloadAction())
        })
        .catch(error => {

        })
    }
}

actions.friendRequestWillSendAction = (fromId, toId) => {
    if(fromId === null || fromId === undefined || toId === null || toId === undefined) return null;
    return dispatch => {
        dispatch({
            type: Types.FRIEND_REQUEST_WILL_SEND,
            fromId,
            toId
        })

        SomaApi.shared.post('/user/friend_request', {
            fromId,
            toId
        })
        .then(response => {
            dispatch(sessionActions.sessionUserWillReloadAction(function(error) {
                dispatch({
                    type: Types.FRIEND_REQUEST_DID_SEND,
                    fromId,
                    toId
                })
            }))
        })
        .catch(error => {
            dispatch({
                type: Types.FRIEND_REQUEST_DID_SEND,
                fromId,
                toId,
                error
            })
        })
    }
}

actions.userProfilesWillLoadAction = (filter, populate) => {
    return dispatch => {
        dispatch({
            type: Types.USER_PROFILE_WILL_LOAD
        })

        SomaApi.shared.request().data("GET", "user", "users", {
            filter,
            populate
        })
        .then(response => {
            dispatch({
                type: Types.USER_PROFILE_DID_LOAD,
                data: response.data
            })
        })
        .catch(error => {
            dispatch({
                type: Types.USER_PROFILE_DID_LOAD,
                error
            })
        })
    }
}

actions.clearUserProfileErrors = () => {
    return dispatch => {
        dispatch({
            "type": Types.USER_PROFILE_CLEAR_ERROR
        })
    }
}

actions.userProfileWillCreateAction = (userProfile) => {
    return dispatch => {
        if(!userProfile) return null
        userProfile = Object.assign({}, userProfile, {"_id": undefined})
        dispatch({
            "type": Types.USER_PROFILE_WILL_CREATE,
            userProfile
        })

        SomaApi.shared.signUp(userProfile)
            .then(response => {
                dispatch({
                    "type": Types.USER_PROFILE_DID_CREATE,
                    "userProfile": response.data.user
                })
            })
            .catch(error => {
                dispatch({
                    "type": Types.USER_PROFILE_DID_CREATE,
                    "error": error.response.data
                })
            })
    }
}

export default actions