import { combineReducers } from "redux"

import userSessionReducer from "../UserSession/reducer"
import userProfileReducer from "../UserProfile/reducer"
import chatReducer from "../Chat/reducer"
import searchReducer from "../Search/reducer"
import postReducer from "../Post/reducer"
import alertReducer from "../Alert/reducer"
import notificationReducer from "../Notification/reducer"

const stateReducer = {
    session: userSessionReducer,
    userProfiles: userProfileReducer,
    chat: chatReducer,
    search: searchReducer,
    posts: postReducer,
    alerts: alertReducer,
    notifications: notificationReducer
}

const rootReducer = combineReducers(stateReducer)

export default rootReducer