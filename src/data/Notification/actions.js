export const Types = {
    NOTIFICATION_DID_UPDATE: "NOTIFICATION_DID_PUSH"
}

export default {
    updateNotification: (notification) => {
        return dispatch => {
            dispatch({
                type: Types.NOTIFICATION_DID_UPDATE,
                data: notification
            })
        }
    }
}