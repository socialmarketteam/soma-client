import {Types} from "./actions"

const initialState = {
    messages: [],
    news: []
}

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case Types.NOTIFICATION_DID_UPDATE:
            var notification = action.data || {};
            return Object.assign({}, state, {
                messages: notification.messages || [],
                news: notification.news || []
            })
        default:
            return state;
    }
}