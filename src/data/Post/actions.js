import SomaApi from "../../SomaApi";
import alertActions from "../Alert/actions"
import uuid from "uuid/v4"
import Firebase from "../../firebase";
import moment from "moment";

export const Types = {
    POST_WILL_CREATE:"POST_WILL_CREATE",
    POST_DID_CREATE:"POST_DID_CREATE",
    NEWS_FEED_WILL_FETCH: "NEWS_FEED_WILL_FETCH",
    NEWS_FEED_DID_FETCH: "NEWS_FEED_DID_FETCH",
    POST_WILL_UPDATE: "POST_WILL_UPDATE",
    POST_DID_UPDATE: "POST_DID_UPDATE",
    POST_WILL_LOAD: "POST_WILL_LOAD",
    POST_DID_LOAD: "POST_DID_LOAD"
}

export default {
    createPost: (post, creator) => {
        return dispatch => {
            dispatch({
                type: Types.POST_WILL_CREATE
            })

            SomaApi.shared.request().data("POST", "post", "posts", undefined, post)
            .then(response => {
                dispatch({
                    type: Types.POST_DID_CREATE,
                    data: Object.assign({}, response.data, {
                        creator
                    })
                })
            })
            .catch(error => {
                dispatch({
                    type: Types.POST_DID_CREATE,
                    error
                })
                dispatch(alertActions.pushAlert({
                    uuid: uuid(),
                    message: "Something went wrong. Please try again later",
                    type: "danger"
                }))
            })
        }
    },
    fetchNewsfeed: () => {
        return dispatch => {
            dispatch({
                type: Types.NEWS_FEED_WILL_FETCH
            })
            SomaApi.shared.get('/post/newsfeed')
            .then(response => {
                dispatch({
                    type: Types.NEWS_FEED_DID_FETCH,
                    data: response.data
                })
            })
            .catch(error => {
                dispatch({
                    type: Types.NEWS_FEED_DID_FETCH,
                    error
                })
                dispatch(alertActions.pushAlert({
                    uuid: uuid(),
                    message: "Sorry! We cannot fetch your news feed. Please try again later",
                    type: "danger"
                }))
            })
        }
    },
    getTimeLinePosts: (id) => {
        return dispatch => {
            if(!id) return;
            dispatch({
                type: Types.POST_WILL_LOAD
            })
            SomaApi.shared.get(`/post/timeline/${id}`)
            .then(response => {
                dispatch({
                    type: Types.POST_DID_LOAD,
                    data: response.data
                })
            })
            .catch(error => {
                dispatch({
                    type: Types.POST_DID_LOAD,
                    error
                })
                dispatch(alertActions.pushAlert({
                    uuid: uuid(),
                    message: "Sorry! We cannot fetch your timeline. Please try again later",
                    type: "danger"
                }))
            })
        }
    },
    likePost: (postId, user, creatorId) => {
        return dispatch => {
            if(!postId) return;
            SomaApi.shared.post(`/post/like/${postId}`)
            .then(response => {
                dispatch({
                    type: Types.POST_DID_UPDATE,
                    data: response.data
                })
                if(user && creatorId) {
                    var message = `${user.name} liked your post`;
                    var notification = {
                        type: "NEWS",
                        data: {
                            isNotified: false,
                            isSeen: false,
                            url: `/post/${postId}`,
                            message,
                            sender: user,
                            createdAt: moment.utc().toDate()
                        }
                    }
                    Firebase.firestore.notification.pushNotification(notification, [creatorId])
                }
            })
            .catch(error => {
                dispatch({
                    type: Types.POST_DID_UPDATE,
                    error
                })
                dispatch(alertActions.pushAlert({
                    uuid: uuid(),
                    message: "Sorry! Something. Please try again later",
                    type: "danger"
                }))
            })
        }
    },
    dislikePost: (postId) => {
        return dispatch => {
            if(!postId) return;
            SomaApi.shared.post(`/post/dislike/${postId}`)
            .then(response => {
                dispatch({
                    type: Types.POST_DID_UPDATE,
                    data: response.data
                })
            })
            .catch(error => {
                dispatch({
                    type: Types.POST_DID_UPDATE,
                    error
                })
                dispatch(alertActions.pushAlert({
                    uuid: uuid(),
                    message: "Sorry! Something. Please try again later",
                    type: "danger"
                }))
            })
        }
    },
    comment: (post, comment, user) => {
        return dispatch => {
            if(!post || !post._id || !comment || !comment.content) return;
            dispatch({
                type: Types.POST_WILL_UPDATE
            })
            SomaApi.shared.post(`/post/comment/${post._id}`, comment)
            .then(response => {
                dispatch({
                    type: Types.POST_DID_UPDATE,
                    data: response.data
                })
                if(user && post._id) {
                    var message = `${user.name} commented on your post`;
                    var notification = {
                        type: "NEWS",
                        data: {
                            isNotified: false,
                            isSeen: false,
                            url: `/post/${post._id}`,
                            message,
                            sender: user,
                            createdAt: moment.utc().toDate()
                        }
                    }
                    Firebase.firestore.notification.pushNotification(notification, [post.creator._id])
                }
            })
            .catch(error => {
                dispatch({
                    type: Types.POST_DID_UPDATE,
                    error
                })
                dispatch(alertActions.pushAlert({
                    uuid: uuid(),
                    message: "Sorry! Something. Please try again later",
                    type: "danger"
                }))
            })
        }
    },
    loadPost: (filter) => {
        return dispatch => {
            dispatch({
                type: Types.POST_WILL_LOAD
            })

            SomaApi.shared.request().data("GET", "post", "posts", {
                filter
            })
            .then(response => {
                dispatch({
                    type: Types.POST_DID_LOAD,
                    data: response.data
                })
            })
            .catch(error => {
                dispatch({
                    type: Types.POST_DID_LOAD,
                    error
                })
                dispatch(alertActions.pushAlert({
                    uuid: uuid(),
                    message: "Sorry! We cannot load your post. Please try again later",
                    type: "danger"
                }))
            })
        }
    }
}