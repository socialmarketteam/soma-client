import {Types} from "./actions"

const initialState = {
    data: [],
    isCreating: false,
    isLoading: false,
    isUpdating: false,
    isDeleting: false,
    error: null
}

const postReducer = (state = initialState, action) => {
    switch(action.type) {
        case Types.POST_DID_LOAD:
            return Object.assign({}, state, {
                isLoading: false,
                data: action.data || [],
                error: action.error || null
            })
        case Types.POST_WILL_LOAD:
            return Object.assign({}, state, {
                isLoading: true
            })
        case Types.POST_WILL_UPDATE:
            return Object.assign({}, state, {
                isUpdating: true
            })
        case Types.POST_DID_UPDATE:
            return Object.assign({}, state, {
                isUpdating: false,
                data: action.data ? state.data.map(post => {
                        if(post._id != action.data._id) return post;
                        return action.data
                    })
                    : state.data,
                error: action.error || null
            })
        case Types.NEWS_FEED_WILL_FETCH:
            return Object.assign({}, state, {
                isLoading: true
            })
        case Types.NEWS_FEED_DID_FETCH:
            return Object.assign({}, state, {
                isLoading: false,
                data: action.data || state.data,
                error: action.error || null
            })
        case Types.POST_WILL_CREATE: 
            return Object.assign({}, state, {
                isCreating: true
            })
        case Types.POST_DID_CREATE:
            return Object.assign({}, state, {
                isCreating: false,
                data: action.data ? [action.data, ...state.data] : state.data,
                error: action.error || null
            })
        default: return state;
    }
}

export default postReducer