import {Types} from "./actions"

const initialState = {
    searchResult: {},
    isSearching: false,
    error: null
}

export default (state = initialState, action = {}) => {
    switch(action.type) {
        case Types.SEARCH_RESULT_WILL_LOAD: 
            return Object.assign({}, state, {
                isSearching: true,
                searchResult: {},
                error: null
            })
        case Types.SEARCH_RESULT_DID_LOAD:
            return Object.assign({}, state, {
                isSearching: false,
                searchResult: action.data || {},
                error: action.error || null
            })
        default:
            return state
    }
}