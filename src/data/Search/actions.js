import SomaApi from "../../SomaApi"

export const Types = {
    SEARCH_RESULT_WILL_LOAD: "SEARCH_RESULT_WILL_LOAD",
    SEARCH_RESULT_DID_LOAD: "SEARCH_RESULT_DID_LOAD"
}

var actions = {};

actions.search = (searchText) => {
    return dispatch => {
        dispatch({
            type: Types.SEARCH_RESULT_WILL_LOAD
        })

        SomaApi.shared.get('/search', {
            s: searchText
        }).then(response => {
            dispatch({
                type: Types.SEARCH_RESULT_DID_LOAD,
                data: response.data
            })
        }).catch(error => {
            dispatch({
                type: Types.SEARCH_RESULT_DID_LOAD,
                error
            })
        })
    }
}

export default actions