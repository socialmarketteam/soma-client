import action from "./actions"
import reducer from "./reducer"

export default {
    action,
    reducer
}