import SomaApi from "../../SomaApi"

export const Types = {
    USER_SESSION_WILL_CREATE: "USER_SESSION_WILL_CREATE",
    USER_SESSION_DID_CREATE: "USER_SESSION_DID_CREATE",
    USER_SESSION_CLEAR_ERROR: "USER_SESSION_CLEAR_ERROR",
    RESET_PASSWORD_EMAIL_WILL_SEND: "RESET_PASSWORD_EMAIL_WILL_SEND",
    RESET_PASSWORD_EMAIL_DID_SEND: "RESET_PASSWORD_EMAIL_DID_SEND",
    PASSWORD_WILL_RESET: "PASSWORD_WILL_RESET",
    PASSWORD_DID_RESET: "PASSWORD_DID_RESET",
    USER_SESSION_DID_DELETE: "USER_SESSION_DID_DELETE",
    SESSION_USER_WILL_RELOAD: "SESSION_USER_WILL_RELOAD",
    SESSION_USER_DID_RELOAD: "SESSION_USER_DID_RELOAD",
    SESSION_USER_WILL_UPDATE: "SESSION_USER_WILL_UPDATE",
    SESSION_USER_DID_UPDATE: "SESSION_USER_DID_UPDATE"
}

var actions = {}

actions.sessionUserWillUpdateAction = (userData) => {
    if(!userData) return null;
    return dispatch => {        
        dispatch({
            type: Types.SESSION_USER_DID_UPDATE,
            data: userData
        })
    }
}

actions.clearUserSessionErrors = () => {
    return dispatch => {
        dispatch({
            "type": Types.USER_SESSION_CLEAR_ERROR
        })
    }
}

actions.sessionUserWillReloadAction = (callback) => {
    return dispatch => {

        dispatch({
            type: Types.SESSION_USER_WILL_RELOAD
        })

        SomaApi.shared.get(`/user/refresh_session`)
        .then(response => {
            dispatch({
                type: Types.SESSION_USER_DID_RELOAD,
                user: response.data
            })
            if(callback) callback()
        })
        .catch(error => {
            dispatch({
                type: Types.SESSION_USER_DID_RELOAD,
                error
            })
            if(callback) callback(error)
        })
    }
}

actions.logoutAction = () => {
    return dispatch => {
        SomaApi.shared.logout()
        .then(() => {
            dispatch({
                type: Types.USER_SESSION_DID_DELETE
            })
        })
        .catch(error => {
            dispatch({
                type: Types.USER_SESSION_DID_DELETE
            })
        })
        
    }
}

actions.loginAction = (account) => {
    return dispatch => {
        if(!account) return null
        account = Object.assign({}, account, {"_id": undefined})
        dispatch({
            "type": Types.USER_SESSION_WILL_CREATE
        })

        SomaApi.shared.login(account)
            .then(response => {
                dispatch({
                    "type": Types.USER_SESSION_DID_CREATE,
                    "session": response.data
                })
            })
            .catch(error => {
                dispatch({
                    "type": Types.USER_SESSION_DID_CREATE,
                    "error": error.response ? error.response.data : error
                })
            })
    }
}

actions.sendResetPasswordEmailAction = (email) => {
    return dispatch => {
        dispatch({
            "type": Types.RESET_PASSWORD_EMAIL_WILL_SEND,
            "account": null
        })

        SomaApi.shared.api.post("/user/reset-password", {"email": email})
            .then(response => {
                dispatch({
                    "type": Types.RESET_PASSWORD_EMAIL_DID_SEND,
                    "resetCode": response.data.resetCode
                })
            })
            .catch(error => {
                dispatch({
                    "type": Types.RESET_PASSWORD_EMAIL_DID_SEND,
                    "error": error.response ? error.response.data : error
                })
            })
    }
}

actions.resetPassword = (newPassword, code) => {
    return dispatch => {
        dispatch({
            "type": Types.PASSWORD_WILL_RESET,
            "account": null
        })

        SomaApi.shared.api.post("/user/reset-password", {"newPassword": newPassword, "code": code})
            .then(response => {
                dispatch({
                    "type": Types.PASSWORD_DID_RESET,
                    "account": response.data
                })
            })
            .catch(error => {
                dispatch({
                    "type": Types.PASSWORD_DID_RESET,
                    "error": error.response ? error.response.data : error
                })
            })
    }
}

export default actions