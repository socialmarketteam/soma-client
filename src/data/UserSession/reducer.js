import { Types } from "./actions"

const initState = {
    "sessionToken": null,
    "user": null,
    "account": null,
    "network": {
        "errors": []
    },
    "isCreating": false,
    "isLoadingFriends": false,
    "isLoading": false
}

const userSessionReducer = (state = initState, action) => {
    switch (action.type) {
        case Types.USER_FRIENDS_WILL_LOAD:
            return Object.assign({}, state, {
                isLoadingFriends: true
            })
        case Types.USER_FRIENDS_DID_LOAD:
            return Object.assign({}, state, {
                isLoadingFriends: false,
                user: Object.assign({}, state.user, {
                    friends: action.data || state.user.friends
                }),
                network: Object.assign({}, state.network, {
                    errors: action.error ? [...state.network.errors, action.error] : []
                })
            })
        case Types.SESSION_USER_DID_UPDATE:
            return Object.assign({}, state, {
                user: action.data ? Object.assign({}, state.user, action.data) : state.user
            })
        case Types.SESSION_USER_WILL_RELOAD:
            return Object.assign({}, state, {
                isLoading: true
            })
        case Types.SESSION_USER_DID_RELOAD:
            if(action.error || !action.user) return initState;
            return Object.assign({}, state, {
                isLoading: false,
                user: action.user || state.user
            })
        case Types.USER_SESSION_DID_DELETE:
            return initState
        case Types.USER_SESSION_WILL_CREATE:
            return Object.assign({}, state, {
                "isCreating": true,
                "network": {
                    "errors": []
                }
            })
        case Types.USER_SESSION_DID_CREATE:
            return Object.assign({}, state, {
                "sessionToken": action.session ? action.session.session_token : null,
                "user": action.session ? action.session.user : null,
                "account": action.session ? action.session.account : null,
                "network": {
                    "errors": action.error ? [...state.network.errors, action.error] : state.network.errors
                },
                "isCreating": false
            })
        case Types.USER_SESSION_CLEAR_ERROR:
            return Object.assign({}, state, {
                "network": {
                    "errors": []
                }
            })
        case Types.RESET_PASSWORD_EMAIL_WILL_SEND:
            return Object.assign({}, state, {
                "account": action.account,
                "network": {
                    "errors": []
                }
            })
        case Types.RESET_PASSWORD_EMAIL_DID_SEND:
            return Object.assign({}, state, {
                "account": Object.assign({}, state.account, { "reset_code": action.resetCode ? action.resetCode : undefined }),
                "network": {
                    "errors": action.error ? [...state.network.errors, action.error] : state.network.errors
                }
            })
        case Types.PASSWORD_WILL_RESET:
            return Object.assign({}, state, {
                "account": action.account,
                "network": {
                    "errors": []
                }
            })
        case Types.PASSWORD_DID_RESET:
            return Object.assign({}, state, {
                "account": action.account,
                "network": {
                    "errors": action.error ? [...state.network.errors, action.error] : state.network.errors
                }
            })
        case "INIT":
            return initState;
        default:
            return state
    }
}

export default userSessionReducer