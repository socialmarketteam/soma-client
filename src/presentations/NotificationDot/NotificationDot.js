import React from "react"

export default class NotificationDot extends React.Component {
    render() {
        return (
            <div className={`notification-dot ${this.props.notificationCount ? "" : "d-none"}`}>{this.props.notificationCount || ""}</div>
        )
    }
}