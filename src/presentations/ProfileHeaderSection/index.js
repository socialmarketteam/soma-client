import {connect} from "react-redux"
import ProfileHeaderSection from "./ProfileHeaderSection"
import actions from "../../data/UserProfile/actions"
import sessionActions from "../../data/UserSession/actions"

const mapStateToProps = (state) => {
    return {
        session: state.session
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        sendFriendRequest: (fromId, toId) => {
            dispatch(actions.friendRequestWillSendAction(fromId, toId))
        },
        cancelFriendRequest: (fromId, toId) => {
            dispatch(actions.friendRequestWillCancelAction(fromId, toId))
        },
        refreshSession: () => {
            dispatch(sessionActions.sessionUserWillReloadAction())
        },
        acceptFriendRequest: (fromId) => {
            dispatch(actions.acceptFriendRequest(fromId))
        },
        unfriend: (userId) => {
            dispatch(actions.unfriend(userId))
        }
    }
}

const ProfileHeaderSectionContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ProfileHeaderSection)

export default ProfileHeaderSectionContainer