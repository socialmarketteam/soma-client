import React, {Component} from "react";
import PropTypes from "prop-types";

const defaultState = (props) => {
    return {
        friendRequestSent: (props.userProfile && props.session.user.sent_friend_requests.find(requestId => requestId === props.userProfile._id)) ? true : false,
        isSendingFriendRequest: false
    }
}

class RelationshipContainer extends Component {
    constructor(props) {
        super(props)
        this.state = defaultState(props)
    }

    addFriend(event) {
        if(event) event.preventDefault()
        this.setState({
            isSendingFriendRequest: true
        })
        this.props.sendFriendRequest(this.props.session.user._id, this.props.userProfile._id)
    }

    cancelFriendRequest(event) {
        if(event) event.preventDefault()
        this.setState({
            friendRequestSent: false
        })
        this.props.cancelFriendRequest(this.props.session.user._id, this.props.userProfile._id)
    }

    acceptFriendRequest(event) {
        if(event) event.preventDefault()
        this.props.acceptFriendRequest(this.props.userProfile._id)
    }

    deleteFriendRequest(event) {
        if(event) event.preventDefault()
        this.props.cancelFriendRequest(this.props.userProfile._id,this.props.session.user._id)
    }

    unfriend(event) {
        if(event) event.preventDefault()
        this.props.unfriend(this.props.userProfile._id)
    }

    componentWillMount() {
        this.props.refreshSession()
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            friendRequestSent: (nextProps.userProfile && nextProps.session.user && nextProps.session.user.sent_friend_requests.find(request => request._id === nextProps.userProfile._id)) ? true : false
        })
    }

    render() {
        if(!this.props.session.user) return null;
        return (
            this.props.userProfile && this.props.userProfile._id !== this.props.session.user._id
            ?
                this.props.session.user.friends.find(friend => friend._id === this.props.userProfile._id) ? 
                    <div className="relationship-container">
                        <button 
                            className="btn"
                            onClick={this.unfriend.bind(this)}
                        >Unfriend</button>
                    </div>
                :
                <div className="relationship-container">
                    {this.props.session.user.friend_requests.find(request => request._id === this.props.userProfile._id) ? 
                        <div>
                            <button 
                                className="btn"
                                onClick={this.acceptFriendRequest.bind(this)}
                            >Accept</button>
                            <button
                                className="btn"
                                onClick={this.deleteFriendRequest.bind(this)}
                            >Delete</button>
                        </div>
                    :
                        this.state.friendRequestSent 
                        ?
                            <button 
                                className="btn"
                                onClick={this.cancelFriendRequest.bind(this)}
                            >Cancel</button>
                        :
                            <button 
                                className="btn"
                                onClick={this.addFriend.bind(this)}
                            >Add Friend</button>
                    }
                </div>
            
            : null
        )
    }
}

RelationshipContainer.propTypes = {
    session: PropTypes.any,
    userProfile: PropTypes.any,
    sendFriendRequest: PropTypes.func
}

export default RelationshipContainer