import React, { Component } from "react"
import CoverPhotoSection from "../CoverPhotoSection"
import AvatarSection from "../AvatarSection"
import ProfileHeaderSectionNavigator from "./components/ProfileHeaderSectionNavigator"
import RelationshipContainer from "./components/RelationshipContainer"
import PropTypes from "prop-types"

class ProfileHeaderSection extends Component {
    render() {
        return (
            <section className="profile-header">
                <CoverPhotoSection profile={this.props.profile} {...this.props} />
                <AvatarSection {...this.props}/>
                <ProfileHeaderSectionNavigator />
                <RelationshipContainer {...this.props} userProfile={this.props.profile} />
            </section>
        )
    }
}

ProfileHeaderSection.propTypes = {
    session: PropTypes.any,
    profile: PropTypes.any
}

export default ProfileHeaderSection