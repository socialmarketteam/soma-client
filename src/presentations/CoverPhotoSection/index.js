import {connect} from "react-redux"
import CoverPhotoSection from "./CoverPhotoSection"
import alertActions from "../../data/Alert/actions"
import userProfileActions from "../../data/UserProfile/actions"

const mapStateToProps = (state) => {
    return {
        session: state.session
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        "updateProfile": (userProfile) => {
            dispatch(userProfileActions.updateProfile(userProfile))
        },
        "pushAlert": (alert) => {
            dispatch(alertActions.pushAlert(alert))
        }
    }
}

const CoverPhotoSectionContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CoverPhotoSection)

export default CoverPhotoSectionContainer