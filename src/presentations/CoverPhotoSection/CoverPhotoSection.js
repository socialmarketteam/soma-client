import React, {Component} from "react"
import blackCoverImg from "../../assets/images/blank_cover_image.jpg"
import PropTypes from "prop-types"
import FilesApi from "../../SomaApi/FilesApi";

class CoverPhotoSection extends Component {
    constructor(props) {
        super(props),
        this.state = {
            coverPath: props.profile && props.profile.cover_path ? props.profile.cover_path.trim() : blackCoverImg
        }
    }
    
    onCoverPhotoChanged(file) {
        if(file) {
            FilesApi.shared.uploadFile(file, "images")
            .then(response => {
                var fileRecord = response.data;
                this.setState({
                    coverPath: fileRecord.url || this.state.coverPath
                }, () => {
                    this.props.updateProfile(Object.assign({}, this.props.profile, {
                        cover_path: this.state.coverPath
                    }))
                })
            })
            .catch(error => {
                this.props.pushAlert({
                    message: "Sorry! We cannot update your cover photo. Please try again later",
                    type: "danger"
                })
            })
        }
    }
    render() {
        return (
            <div className={`${this.props.className || ""} cover-photo-section`}  style={{overflow: "hidden"}}>
                <div id="changeCoverPhotoLayer" className={`change-cover-photo-layer${this.props.session.user && this.props.session.user._id != this.props.profile._id ? "-dnone d-none" : ""}`} onClick={event => {
                        event.preventDefault();
                        var f = document.getElementById('coverPhotoUploadInput')
                        if(f) document.getElementById('changeCoverPhotoLayer').removeChild(f)
                        f=document.createElement('input');
                        f.id="coverPhotoUploadInput"
                        f.style.display='none';
                        f.type='file';
                        f.name='file';
                        f.accept='image/*';
                        f.onchange = ev => {
                            var file = ev.target.files[0];
                            this.onCoverPhotoChanged(file)
                        }
                        document.getElementById('changeCoverPhotoLayer').appendChild(f);
                        f.click();
                    }}>
                    <h6>Change Cover Photo</h6>
                </div>
                <a className="clickable" onClick={() => {
                    location.assign(this.state.coverPath)
                }}>
                    <img src={this.state.coverPath} height="100%" width={`100%`} />
                </a>
            </div>
        )
    }
}

CoverPhotoSection.propsTypes = {
    session: PropTypes.any,
    profile: PropTypes.any
}

export default CoverPhotoSection