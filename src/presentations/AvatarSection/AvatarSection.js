import React, {Component} from "react"
import user_maleImg from "../../assets/images/user_male.jpg"
import user_femaleImg from "../../assets/images/user_female.jpg"
import ReactCrop from 'react-image-crop'
import 'react-image-crop/dist/ReactCrop.css'
import FilesApi from "../../SomaApi/FilesApi";


class AvatarSection extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isEditingName: false,
            isUpdating: false,
            name: props.profile ? props.profile.name : "",
            avatarFile: props.profile.avatar_path || (props.profile.gender == "female" ? user_femaleImg : user_maleImg),
            avatarFileToUpdate: null,
            crop: {
                aspect: 1/1,
                width: 50,
                height: 50
            },
            imageFile: null
        }
    }
    componentWillReceiveProps(nextProps) {
        if(this.state.isUpdating && !nextProps.userProfiles.isUpdating){
            this.setState({
                isUpdating: false
            })
            this.forceUpdate()
        }
    }
    updateProfile() {
        this.setState({
            isUpdating: true
        })
        this.props.updateProfile(Object.assign({}, this.props.profile, {
            name: this.state.name,
            avatar_path: this.state.avatarFileToUpdate || this.props.profile.avatar_path
        }))
    }
    onAvatarChanged(file) {
        if(file) {
            // this.setState({
            //     imageFile: file
            // })
            // var reader = new FileReader;
            // reader.onload = e => {
            //     this.setState({
            //         avatarPreview: e.target.result
            //     })
            // }
            // reader.readAsDataURL(file)
            FilesApi.shared.uploadFile(file, 'images')
            .then(response => {
                var fileRecord = response.data;
                if(fileRecord) {
                    this.setState({
                        avatarFileToUpdate: fileRecord.url || this.state.avatarFile,
                        avatarFile: fileRecord.url || this.state.avatarFile
                    }, () => {
                        this.updateProfile();
                    })
                } else {
                    this.props.pushAlert({
                        uuid: uuid(),
                        message: "Sorry! We cannot update your profile image. Please try again later",
                        type: "danger"
                    })
                }
            })
            .catch(error => {
                console.log(error)
                this.props.pushAlert({
                    uuid: uuid(),
                    message: "Sorry! We cannot update your profile image. Please try again later",
                    type: "danger"
                })
            })
        }
    }
    cropImageAndSave() {
        const canvas = document.createElement('canvas');
        canvas.width = this.state.crop.width;
        canvas.height = this.state.crop.width;
        const ctx = canvas.getContext('2d');

        var img = document.getElementsByClassName('ReactCrop__image')[0]
        ctx.drawImage(
            img,
            this.state.crop.x || 0,
            this.state.crop.y || 0,
            this.state.crop.width,
            this.state.crop.width,
            0,
            0,
            this.state.crop.width,
            this.state.crop.width
        )

        const imageUrl = canvas.toDataURL('image/png')
        this.setState({
            avatarFile: imageUrl,
            avatarPreview: null
        })
        canvas.toBlob((blob => {
            blob.name = this.state.imageFile.name;
        }), "image/jpeg")
    }
    render() {
        const profile = this.props.profile || {};
        return (
            <div id="avatarSection">
                <div className={`soma-modal ${this.state.avatarPreview ? "" : 'd-none'}`}>
                    <div className="soma-modal-body">
                        <div className="col soma-modal-header">
                            <strong style={{fontSize: "1.2rem"}}>Crop Image</strong>
                            <span style={{fontSize: "1.2rem"}} className="float-right clickable" onClick={() => {
                                this.setState({
                                    avatarPreview: null
                                })
                            }}>&times;</span>
                        </div>
                        <div className="col p-3">
                            <ReactCrop src={this.state.avatarPreview} crop={this.state.crop} onChange={(crop) => {
                                this.setState({
                                    crop
                                })
                            }} />
                        </div>
                        <div className="col soma-modal-footer">
                            <button type="button" className="btn btn-secondary" onClick={event => {
                                event.preventDefault()
                                this.setState({
                                    avatarPreview: null
                                })
                            }}>
                                Cancel
                            </button>
                            <button className="btn btn-primary float-right" onClick={event => {
                                event.preventDefault()
                                this.cropImageAndSave()
                            }}>
                                Save
                            </button>
                        </div>
                    </div>
                </div>
                <div className="profile-avatar clickable">
                    <img src={this.state.avatarFile} width="100%" height="100%" onClick={event => {
                        event.preventDefault();
                        location.assign(this.state.avatarFile)
                    }}/>
                    <div id="changeAvatarLayer" className={`change-avatar-layer${this.props.session.user && this.props.session.user._id != profile._id ? "-dnone d-none" : ""}`} onClick={event => {
                        event.preventDefault();
                        // if(document.getElementById('avatarUploadInput')) return;
                        var f = document.getElementById('avatarUploadInput')
                        if(f) document.getElementById('changeAvatarLayer').removeChild(f)
                        f=document.createElement('input');
                        f.id="avatarUploadInput"
                        f.style.display='none';
                        f.type='file';
                        f.name='file';
                        f.accept='image/*';
                        f.onchange = ev => {
                            var file = ev.target.files[0];
                            this.onAvatarChanged(file)
                        }
                        document.getElementById('changeAvatarLayer').appendChild(f);
                        f.click();
                    }}>
                        <h6>Change Image</h6>
                    </div> 
                </div>
                <div className="profile-name">
                    <h4>{this.state.isEditingName ? <input id="editProfileNameInput" className="no-border no-background text-white" value={this.state.name} onChange={event => {
                        event.preventDefault();
                        this.setState({
                            name: event.target.value
                        })
                    }} /> 
                    : this.state.name}&nbsp;&nbsp;
                    {this.state.isEditingName ? 
                    <small className={`clickable hover-underline`} style={{fontSize: "1rem"}}  onClick={event=> {
                        event.preventDefault();
                        this.setState({
                            isEditingName: false
                        })
                        this.updateProfile();
                    }}>
                    <i className="fa fa-check" aria-hidden="true"></i>
                    </small>
                    : 
                    <small className={`clickable hover-underline ${this.props.session.user && this.props.session.user._id != profile._id ? 'd-none': ''}`} style={{fontSize: "1rem"}}  onClick={event=> {
                        event.preventDefault();
                        this.setState({
                            isEditingName: true
                        })
                    }}>
                    <i className="fa fa-pencil" aria-hidden="true"></i>
                    </small>}
                    </h4>
                </div>
            </div>
        )
    }
}

export default AvatarSection