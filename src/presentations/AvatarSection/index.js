import {connect} from "react-redux"
import AvatarSection from "./AvatarSection"
import alertActions from "../../data/Alert/actions"
import userProfileActions from "../../data/UserProfile/actions"

const mapStateToProps = (state) => {
    return state
}
const mapDispatchToProps = (dispatch) => {
    return {
        "updateProfile": (userProfile) => {
            dispatch(userProfileActions.updateProfile(userProfile))
        },
        "pushAlert": (alert) => {
            dispatch(alertActions.pushAlert(alert))
        }
    }
}

const AvatarSectionContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(AvatarSection)

export default AvatarSectionContainer;