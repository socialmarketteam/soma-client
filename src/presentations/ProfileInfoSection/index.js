import {connect} from "react-redux"
import ProfileInfoSection from "./ProfileInfoSection"
import userProfileActions from "../../data/UserProfile/actions"

const mapStateToProps = (state) => {
    return state
}

const mapDispatchToProps = dispatch => {
    return {
        "updateProfile": (userProfile) => {
            dispatch(userProfileActions.updateProfile(userProfile))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProfileInfoSection)