import React from "react"
import moment from "moment";
import inflection from "inflection"
import DatePicker from "react-datepicker"

export default class ProfileInfoSection extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            email: props.profile.email || "",
            phone: props.profile.phone || "",
            birth_date: props.profile.birth_date || "",
            gender: props.gender || "male",
            isUpdating: false
        }
    }
    componentWillReceiveProps(nextProps) {
        if(this.state.isUpdating && !nextProps.userProfiles.isUpdating) {
            location.reload();
        }
    }

    updateProfile() {
        this.setState({
            isUpdating: true
        })
        this.props.updateProfile(Object.assign({}, this.props.profile, {
            email: this.state.email,
            phone: this.state.phone,
            birth_date: this.state.birth_date,
            gender: this.state.gender
        }))
    }

    render() {
        const profile = this.props.profile || {};
        return (
            <div>
                <div id="updateProfileModal" className="modal fade" role="dialog">
                    <div className="modal-dialog">
                        <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title">Update Profile</h4>
                            <button type="button" className="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div className="modal-body">
                            <div className="form-group">
                                <label>Email</label>
                                <input className="form-control" type="text" value={this.state.email} onChange={event => {
                                    this.setState({
                                        email: event.target.value
                                    })
                                }} />
                            </div>
                            <div className="form-group">
                                <label>Phone Number</label>
                                <input className="form-control" type="number" value={this.state.phone} onChange={event => {
                                    this.setState({
                                        phone: event.target.value
                                    })
                                }} />
                            </div>
                            <div className="form-group">
                                <label>Date of Birth</label>
                                <input type="date" className="form-control" value={this.state.birth_date} onChange={event => {
                                    this.setState({
                                        birth_date: event.target.value
                                    })
                                }} />
                            </div>
                            <div className="form-group">
                                <label>Gender</label>
                                <select className="form-control" value={this.state.gender} onChange={event => {
                                    this.setState({
                                        gender: event.target.value
                                    })
                                }}>
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                                </select>
                            </div>
                        </div>
                        <div className="modal-footer justify-content-between">
                            <button type="button" className="btn btn-default" data-dismiss="modal" disabled={this.state.isUpdating}>Close</button>
                            <button type="button" className="btn btn-primary" disabled={this.state.isUpdating} onClick={event => {
                                event.preventDefault();
                                this.updateProfile();
                            }}>Save</button>                            
                        </div>
                        </div>

                    </div>
                </div>
                <div className={`card ${this.props.containerClassName || ""}`}>
                    <div className="card-header">
                        Profile
                        <button className="float-right btn btn-link p-0" data-toggle="modal" data-target="#updateProfileModal"><small><i className="fa fa-pencil" aria-hidden="true"></i></small></button>
                    </div>
                    <div className="card-body"> 
                        <div className="mb-2">
                            <p>Email:&nbsp;{profile.email ? <a href={`mailto:${profile.email}`}>{profile.email}</a> : '-'}</p>
                        </div>
                        <div className="mb-2">
                            <p>Phone number:&nbsp;{profile.phone ? <a href={`callto:${profile.phone}`}>{profile.phone || "-"}</a> : '---'}</p>
                        </div>
                        <div className="mb-2">
                            <p>Date of Birth:&nbsp;{profile.birth_date ? moment(profile.birth_date).format('YYYY-MM-DD') : '---'}</p>
                        </div>
                        <div className="mb-2">
                            <p>Gender:&nbsp;{profile.gender ? inflection.humanize(profile.gender) : '---'}</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}