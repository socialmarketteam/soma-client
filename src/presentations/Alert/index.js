import React from "react"
import PropTypes from "prop-types"
import "./alert.css"

export default class Alert extends React.Component {

    render() {
        if(!this.props.alerts.alerts.length) return null;
        return (
            <ul className="alert-container">
                {this.props.alerts.alerts.map(alert => {
                    return (
                        <li key={alert.uuid} className={`alert alert-${alert.type}`}>
                            <div className="col-12">
                                <span className="float-right alert-text clickable" onClick={event => {
                                    event.preventDefault();
                                    this.props.removeAlert(alert.uuid)
                                }}>&times;</span>
                                <p className="alert-text alert-text-container">{alert.message}</p>
                            </div>
                        </li>
                    )
                })}
            </ul>
        )
    }
}

Alert.propTypes = {
    alerts: PropTypes.any,
    pushAlert: PropTypes.func,
    removeAlert: PropTypes.func
}