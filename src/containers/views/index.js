import RootView from "./RootView"
import {connect} from "react-redux"
import userSessionActions from "../../data/UserSession/actions"
import chatActions from "../../data/Chat/actions"
import notificationActions from "../../data/Notification/actions"

const mapStateToProps = (state) => {
    return state
}

const mapDispatchToProps = (dispatch) => {
    return {
        "updateFriendRequests": (friendRequests) => {
            let fromUsers = friendRequests.map(request => request.from)
            let userDataToUpdate = {
                friend_requests: fromUsers
            }
            dispatch(userSessionActions.sessionUserWillUpdateAction(userDataToUpdate))
        },
        "addChatbox": (conversationId) => {
            dispatch(chatActions.popUpChatbox(conversationId))
        },
        "loadConversation": (conversation) => {
            dispatch(chatActions.loadConversation(conversation))
        },
        "updateNotification": (notification) => {
            dispatch(notificationActions.updateNotification(notification))
        }
    }
}

const RootViewContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(RootView)

export default RootViewContainer