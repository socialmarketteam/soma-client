import {connect} from "react-redux"
import Profile from "./Profile"
import actions from "../../../../data/UserProfile/actions"
import postActions from "../../../../data/Post/actions"

const mapStateToProps = (state) => {
    return {
        session: state.session
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadUserProfiles: (filter, populate) => {
            dispatch(actions.userProfilesWillLoadAction(filter,populate))
        },
        loadTimelinePosts: (id) => {
            dispatch(postActions.getTimeLinePosts(id))
        }
    }
}

const ProfileContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile)

export default ProfileContainer