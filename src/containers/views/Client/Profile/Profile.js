import React, {Component} from "react"
import Client from "../../../layouts/client"
import ProfileHeader from "../../../../presentations/ProfileHeaderSection"
import PropTypes from "prop-types"
import PostList from "../../../layouts/client/components/PostList";
import PostComposer from "../../../layouts/client/components/PostComposer";
import ProfileInfoSection from "../../../../presentations/ProfileInfoSection";

const defaultState = (props) => {
    return {
        userId: null,
        profile: props.profile || {},
        isLoadingUserProfile: false
    }
}

class Profile extends Component {
    constructor(props) {
        super(props)
        this.state = defaultState(props)
    }

    componentWillMount() {
        if(!this.state.profile._id) {
            let userId = this.props.match.params._id
            if(userId !== undefined && userId !== null) {
                this.loadUserProfile(userId)
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if(this.state.isLoadingUserProfile && !nextProps.userProfiles.isLoading) {
            const matchingUser = nextProps.userProfiles.data.find(user => user._id == this.state.userId)
            this.setState({
                isLoadingUserProfile: false,
                profile: matchingUser || {}
            })
            this.props.loadTimelinePosts(matchingUser._id)
        }
        if(nextProps.match.params._id && this.state.userId !== nextProps.match.params._id) {
            this.setState({
                isLoadingUserProfile: true
            })
            this.loadUserProfile(nextProps.match.params._id)
        }
    }

    loadUserProfile(_id) {
        this.setState({
            userId: _id,
            isLoadingUserProfile: true
        })
        this.props.loadUserProfiles(JSON.stringify({
            _id
        }), "friends,sent_friend_requests")
    }

    render() {
        return (
            <Client {...this.props}>
                {this.state.profile._id !== undefined && this.state.profile._id !== null ? 
                    <div className="container">
                       <ProfileHeader profile={this.state.profile} {...this.props} />
                        <section>
                            <div className="row">
                                <div className="col-md-4">
                                    <ProfileInfoSection containerClassName={`mt-3`} profile={this.state.profile} {...this.props} />
                                </div>
                                <div className="col-md-8">
                                    <PostComposer containerClassName={`mt-3 col-12`} creator={this.props.session.user} {...this.props} />
                                    {this.props.posts.isLoading ? 
                                        <div style={{width: "100%", height:'100vh'}}>
                                            <div className="loader" style={{backgroundColor:'transparent'}}></div>
                                        </div>
                                    : 
                                        <PostList {...this.props} />
                                    }
                                </div>
                            </div>
                        </section>     
                    </div>
                : null}
            </Client>
        )
    }
}

Profile.propTypes = {
    profile: PropTypes.any,
    history: PropTypes.any,
    match: PropTypes.any
}

export default Profile