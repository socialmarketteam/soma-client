import { connect } from "react-redux"
import NewsFeed from "./NewsFeed"
import alertActions from "../../../../data/Alert/actions"
import postActions from "../../../../data/Post/actions"

const mapStateToProps = (state) => {
  return {
      "session": state.session,
      "userProfiles": state.userProfiles
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    "pushAlert": (alert) => {
      dispatch(alertActions.pushAlert(alert))
    },
    "removeAlert": (alertUuid) => {
        dispatch(alertActions.removeAlert(alertUuid))
    },
    "fetchNewsFeed": () => {
      dispatch(postActions.fetchNewsfeed())
    }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(NewsFeed)