import React, {Component} from "react"
import Client from "../../../layouts/client"
import PropTypes from "prop-types"
import PostComposer from "../../../layouts/client/components/PostComposer";
import PostList from "../../../layouts/client/components/PostList";

class NewsFeed extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isFetchingNewsFeed: false
        }
    }
    componentWillReceiveProps(nextProps) {
        if(this.state.isFetchingNewsFeed && !nextProps.posts.isLoading) {
            this.setState({
                isFetchingNewsFeed: false
            })
        }
    }
    componentDidMount() {
        this.setState({
            isFetchingNewsFeed: true
        })
        this.props.fetchNewsFeed();
    }

    defaultState(props) {
        return {}
    }

    render() {
        return (
            <Client {...this.props} enableLeftSideBar>
                <div className="m-2">
                    <PostComposer containerClassName="col-12" creator={this.props.session.user} {...this.props} />
                    {this.state.isFetchingNewsFeed ? 
                        <div style={{width: "100%", height:'100vh'}}>
                            <div className="loader" style={{backgroundColor:'transparent'}}></div>
                        </div>
                    : <PostList {...this.props} />}
                </div>

            </Client>
        )
    }
}

NewsFeed.propTypes = {
    "session": PropTypes.object.isRequired
}

export default NewsFeed