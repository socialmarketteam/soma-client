import React from "react"
import Client from "../../../layouts/client"
import SinglePost from "../../../layouts/client/components/SinglePost"

export default class SinglePostModule extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            post: null
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            post: nextProps.posts.data[0]
        })
        if(this.state.isLoading && !nextProps.posts.isLoading) {
            this.setState({
                isLoading: false                
            })
        }
    }

    componentDidMount() {
        var _id=this.props.match.params._id;
        if(_id) {
            this.loadPost(_id)
        }
    }

    loadPost(_id) {
        this.setState({
            isLoading: true
        })
        this.props.loadPost(JSON.stringify({
            _id
        }))
    }
    render() {
        return (
            <Client {...this.props} enableLeftSideBar>
                <div className="m-2">
                    {this.state.isLoading 
                    ? <div style={{width: "100%", height:'100vh'}}>
                        <div className="loader" style={{backgroundColor:'transparent'}}></div>
                    </div>
                    : (this.state.post ? <SinglePost {...this.props} post={this.state.post} /> : null)}
                </div>

            </Client>
        )
    }
}