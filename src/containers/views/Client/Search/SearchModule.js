import React, {Component} from "react"
import Client from "../../../layouts/client";
import querystring from "query-string"
import {Link} from "react-router-dom"
import uuid from "uuid/v4"
import "./searchResult.css"

import maleAvatar from "../../../../assets/images/user_male.jpg"
import feMaleAvatar from "../../../../assets/images/user_female.jpg"

export default class SearchModule extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isSearching: false,
            searchText: ""
        }
    }
    componentDidMount() {
        this.search();
    }
    componentWillReceiveProps(nextProps) {
        if(this.state.isSearching && !nextProps.search.isSearching) {
            this.setState({
                isSearching: false
            })
        }
        if(this.props.location.search != nextProps.location.search) {
            this.search();
        }
    }
    search() {
        var query = querystring.parse(this.props.location.search, {ignoreQueryPrefix: true});
        if(!query.s) return;
        this.setState({
            isSearching: true,
            searchText: query.s
        })
        this.props.doSearch(query.s)
    }
    render() {
        const searchResult = this.props.search.searchResult;
        const users = searchResult["users"] || [];
        const posts = searchResult["posts"] || [];
        return (
            <Client {...this.props}>
                <div className="col-md-12">
                    <div className="col-md-12 pt-3">
                        <div className="card mb-2">
                            <div className="card-header">
                                <strong>People</strong>
                            </div>
                            <div className="card-body">
                                {users.map(user => {
                                    return (
                                    <div key={uuid()} className="row col mb-3 search-result-single">
                                        <div className="search-avatar">
                                            <img className="search-people-avatar" src={user.avatar_path || (user.gender == "female" ? feMaleAvatar : maleAvatar)} />
                                        </div>
                                        <div className="search-info">
                                            <Link to={`/user/${user._id}`}><strong>{user.name}</strong></Link>
                                        </div>
                                    </div>
                                    )
                                })}
                            </div>
                        </div>
                    </div>
                </div>

            </Client>
        )
    }
}