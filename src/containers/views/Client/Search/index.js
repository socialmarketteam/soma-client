import {connect} from "react-redux"
import SearchModule from "./SearchModule"

import searchActions from "../../../../data/Search/actions"

const mapStateToProps = (state) => {
    return state
}
const mapDispatchToProps = (dispatch) => {
    return {
        doSearch: (searchText) => {
            dispatch(searchActions.search(searchText))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SearchModule)