import React, {Component} from "react"
import { Route, Switch, Redirect } from "react-router-dom"
import PropTypes from "prop-types"
import LoginForm from "./components/Login"
import RegisterForm from "./components/Register"
import ForgotPasswordForm from "./components/ForgotPassword"
import ResetPasswordForm from "./components/ResetPassword"

import bg from "../../../assets/pages/login/images/bg-01.jpg"

import "../../../assets/pages/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css"
import "../../../assets/pages/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css"
import "../../../assets/pages/login/vendor/animate/animate.css"
import "../../../assets/pages/login/vendor/css-hamburgers/hamburgers.min.css"
import "../../../assets/pages/login/vendor/animsition/css/animsition.min.css"
import "../../../assets/pages/login/vendor/select2/select2.min.css"
import "../../../assets/pages/login/vendor/daterangepicker/daterangepicker.css"
import "../../../assets/pages/login/css/util.css"
import "../../../assets/pages/login/css/main.css"

class UserSession extends Component {
    constructor(props){
        super(props)
        if(this.props.session.sessionToken) {
            this.props.history.push("/")
        }
    }
    
    render() {
        return(
            <div className="limiter">
                <div className="container-login100" style={{"backgroundImage": `url('${bg}')`}}>
                    <div className="wrap-login100 p-l-110 p-r-110 p-t-62 p-b-33">
                        <div>
                            <Switch>
                                <Route exact path={`${this.props.match.url}`} render={(routerProps) => {
                                    return <LoginForm {...this.props} {...routerProps} />
                                }} />
                                <Route path={`${this.props.match.url}/register`} render={routerProps => {
                                    return <RegisterForm {...this.props} {...routerProps} />}} />
                                <Route path={`${this.props.match.url}/forgot-password`} render={routerProps => {
                                    return <ForgotPasswordForm {...this.props} {...routerProps} />
                                }} />
                                <Route path={`${this.props.match.url}/reset-password/:code`} render={routerProps => {
                                    return <ResetPasswordForm {...this.props} {...routerProps} />
                                }} />
                            </Switch>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

UserSession.propTypes = {
    "session": PropTypes.object.isRequired,
    "login": PropTypes.func.isRequired
}

export default UserSession