import React, {Component} from "react"
import {Link} from "react-router-dom"
import PropTypes from "prop-types"
import loadingGif from "../../../../assets/images/loading.gif"

class Login extends Component {
    constructor(props){
        super(props)
        this.state = Object.assign({}, this.defaultState(props))
    }

    defaultState(props) {
        return {
            "username": undefined,
            "password": undefined,
            "validationErrors": [],
            "session": props.session,
            "isSending": false,
            "nextUrl": undefined
        }
    }

    componentWillReceiveProps(nextProps) {
        if(this.state.isSending && !nextProps.session.isCreating) {
            this.setState({
                "isSending": false
            }, () => {
                if(this.state.nextUrl)
                    this.props.history.push(this.state.nextUrl)
            })
        }
        if(nextProps.session.network.errors.length > 0) {
            this.setState({
                "isSending": false,
                "session": undefined,
                "nextUrl": undefined
            })
        }
    }

    handleSubmit() {
        
        if(this.state.isSending) return

        let validationErrors = []

        if(!this.state.username || this.state.username.trim().length === 0) {
            validationErrors.push("Username cannot be empty")
        }

        if(!this.state.password || this.state.password.trim().length === 0) {
            validationErrors.push("Password cannot be empty")
        }

        if(validationErrors.length > 0) {
            this.setState({
                "validationErrors": validationErrors
            })
            return
        }

        this.props.dismissUserSessionErrors()

        this.props.login(Object.assign({}, {"username": this.state.username, "password": this.state.password}))

        this.setState({
            "isSending": true,
            "nextUrl": "/"
        })
    }

    render() {
        return(
            <form onSubmit={(event) => { 
                    event.preventDefault()
                    this.handleSubmit()
                }} 
                method="post" 
                className="login100-form validate-form flex-sb flex-w"
                >
                <div className="p-t-31 p-b-9">
                    <span className="txt1">
                        Username
                    </span>
                </div>
                <div className="wrap-input100 validate-input">
                    <input 
                        className="input100" 
                        type="text" 
                        name="username"
                        value={this.state.username}
                        onChange={(event) => {
                            this.setState({
                                "username": event.target.value
                            })
                        }}
                    />
                    <span className="focus-input100"></span>
                </div>
            
                <div className="p-t-13 p-b-9">
                    <span className="txt1">
                        Password
                    </span>
                    <Link to="/auth/forgot-password" className="txt2 bo1 m-l-5">
                        Forgot?
                    </Link>
                </div>
                <div className="wrap-input100 validate-input">
                    <input
                        className="input100" 
                        type="password" 
                        name="password"
                        value={this.state.password}
                        onChange={(event) => {
                            this.setState({
                                "password": event.target.value
                            })
                        }}
                    />
                    <span className="focus-input100"></span>
                </div>

                {
                    this.state.validationErrors.length > 0 ? 
                        <div className="p-t-13 p-b-9">
                            <ul>
                                {this.state.validationErrors.map((error, i) => {
                                    return (
                                        <li key={i}><p className="text-danger">{error}</p></li>
                                    )
                                })}
                            </ul>
                        </div>
                    : null
                }

                {
                    this.props.session.network.errors.length > 0 ? 
                        <div className="p-t-13 p-b-9">
                            <ul>
                                {this.props.session.network.errors.map((error, i) => {
                                    var message = JSON.stringify(error.error || error.response || error.message);
                                    return (
                                        <li key={i} className="text-danger">{message}</li>
                                    )
                                })}
                            </ul>
                        </div>
                     : null
                }

                <div className="container-login100-form-btn m-t-17">
                    <button 
                        className="login100-form-btn"
                        disabled={this.state.isSending}
                    >
                            {this.state.isSending ? <img src={loadingGif} height={20} width={20} /> : null}
                            &nbsp;
                            Sign{this.state.isSending ? "ing" : ""} In
                    </button>
                </div>

                <div className="w-full text-center p-t-55">
                    <span className="txt2">
                        Not a member?
                    </span>
                    &nbsp;
                    <Link to="/auth/register" className="txt2 bo1">
                        Sign up now
                    </Link>
                </div>
            </form>
                    
        )
    }
}

Login.propTypes = {
    "session": PropTypes.object.isRequired,
    "history": PropTypes.object.isRequired
}

export default Login