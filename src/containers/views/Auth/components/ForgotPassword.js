import React, {Component} from "react"
import {Link} from "react-router-dom"
import PropTypes from "prop-types"
import stringValidator from "../../../../helpers/stringValidator"

import loadingGif from "../../../../assets/images/loading.gif"

class ForgotPassword extends Component {
    constructor(props){
        super(props)
        this.state = Object.assign({}, this.defaultState(props))
    }

    defaultState(props) {
        return {
            "email": undefined,
            "validationErrors": [],
            "session": props.session,
            "isSending": false,
            "success": false,
            "network": {
                "errors": []
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if(this.state.isSending && nextProps.session.account && nextProps.session.account.reset_code && nextProps.session.account.reset_code.trim().length > 0) {
            this.setState({
                "isSending": false,
                "success": true,
                "network": {
                    "errors": []
                }
            })
        }

        if(nextProps.session.network.errors.length > 0) {
            this.setState({
                "network": {
                    "errors": nextProps.session.network.errors
                },
                "isSending": false
            })
        }
    }

    handleSubmit() {
        let validationErrors = []

        if(!this.state.email || this.state.email.trim().length === 0) {
            validationErrors.push("Email cannot be empty")
        }
        else if(!stringValidator.isValidEmail(this.state.email)) {
            validationErrors.push("Invalid email")
        }

        if(validationErrors.length > 0) {
            this.setState({
                "validationErrors": validationErrors
            })
            return
        }

        this.props.dismissUserSessionErrors()

        this.setState({
            "isSending": true,
            "network": {
                "errors": []
            }
        }, () => {
            this.props.sendResetPasswordEmail(this.state.email)
        })
    }

    render() {
        return(
            <form onSubmit={(event) => { 
                event.preventDefault()
                this.handleSubmit()
            }} 
            method="post" 
            className="login100-form validate-form flex-sb flex-w"
            >
                <div className="p-t-13 p-b-9">
                    <p className="txt2">
                       Enter your email, we will send you a link to reset your password
                    </p>
                </div>
                
                <div className="p-t-13 p-b-9">
                    <p className="txt1">
                        Email
                    </p>
                </div>
                <div className="wrap-input100 validate-input">
                    <input
                        className="input100" 
                        name="email"
                        value={this.state.email}
                        disabled={this.state.isSending}
                        onChange={(event) => {
                            this.setState({
                                "email": event.target.value
                            })
                        }}
                    />
                    <span className="focus-input100"></span>
                </div>

                {
                    this.state.validationErrors.length > 0 ? 
                        <div className="p-t-13 p-b-9">
                            <ul>
                                {this.state.validationErrors.map((error, i) => {
                                    return (
                                        <li key={i}><p className="text-danger">{error}</p></li>
                                    )
                                })}
                            </ul>
                        </div>
                    : null
                }

                {
                    this.state.network.errors.length > 0 ? 
                        <div className="p-t-13 p-b-9">
                            <ul>
                                {this.state.network.errors.map((error, i) => {
                                    return (
                                        <li key={i}><p className="text-danger">{error.error || error}</p></li>
                                    )
                                })}
                            </ul>
                        </div>
                    : null
                }

                {
                    this.state.success ? 
                        <div className="p-t-13 p-b-9">
                            <p className="text-info">Reset password link is sent to your email. Please check and update your password</p>
                        </div>
                    : null
                }

                <div className="container-login100-form-btn m-t-17">
                    <button 
                        className="login100-form-btn"
                        disabled={this.state.isSending}
                    >
                            {this.state.isSending ? <img src={loadingGif} height={20} width={20} /> : null}
                            &nbsp;
                            {this.state.isSending ? "Reseting your password" : "Reset my password"}
                    </button>
                </div>

                <div className="w-full text-center p-t-55">
                    <p>
                        <span className="txt2">
                            Got your password?
                        </span>
                        &nbsp;
                        <Link to="/auth" className="txt2 bo1">
                            Sign in
                        </Link>
                    </p>
                    <p>
                        <Link to="/auth/register" className="txt2 bo1">
                            Sign up now
                        </Link>
                    </p>
                </div>
            </form>
        )
    }
}

ForgotPassword.propTypes = {
    "history": PropTypes.object.isRequired,
    "session": PropTypes.object.isRequired
}

export default ForgotPassword