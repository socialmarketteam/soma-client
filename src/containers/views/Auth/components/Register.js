import React, {Component} from "react"
import {Link} from "react-router-dom"
import PropTypes from "prop-types"
import stringValidator from "../../../../helpers/stringValidator"
import loadingGif from "../../../../assets/images/loading.gif"

const Required = () => {
    return <span className="text-danger">&nbsp;*</span>
}

class Register extends Component {
    constructor(props) {
        super(props)
        this.state = Object.assign({}, this.defaultState(props))
    }

    defaultState(props) {
        return {
            "username": undefined,
            "password": undefined,
            "passwordRetype": undefined,
            "firstName": undefined,
            "lastName": undefined,
            "email": undefined,
            "gender": "male",
            "validationErrors": [],
            "session": props.session,
            "userProfiles": props.userProfiles,
            "isCreatingUserProfile": false,
            "isSending": false,
            "nextUrl": undefined,
            "network": {
                "errors": []
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.userProfiles.me && nextProps.userProfiles.me._id && nextProps.userProfiles.me._id.toString().trim().length > 0 && this.state.isCreatingUserProfile) {
            this.setState({
                "userProfiles": Object.assign({}, this.state.userProfiles, { "me": nextProps.userProfiles.me }),
                "isCreatingUserProfile": false,
                "isSending": false
            }, () => {
                this.handleSubmit()
            })
        }

        if(!this.state.isCreatingUserProfile && nextProps.session.sessionToken && nextProps.session.sessionToken.trim().length > 0) {
            this.setState({
                "isSending": false,
                "session": nextProps.session
            }, () => {
                if(this.state.nextUrl && this.state.nextUrl.trim().length > 0) {
                    this.props.history.push(this.state.nextUrl)
                }
            })
        }

        if(nextProps.session.network.errors.length > 0) {
            this.setState({
                "network": Object.assign({}, this.state.network, {"errors": [...this.state.network.errors, ...nextProps.session.network.errors]}),
                "isSending": false
            })
        }

        if(nextProps.userProfiles.network.errors.length > 0) {
            this.setState({
                "network": Object.assign({}, this.state.network, {"errors": [...this.state.network.errors, ...nextProps.userProfiles.network.errors]}),
                "isSending": false
            })
        }
    }

    handleSubmit() {

        if(this.state.isSending) return

        let validationErrors = []

        if(!this.state.username || this.state.username.trim().length === 0) {
            validationErrors.push("Username cannot be empty")
        }

        if(!this.state.password || this.state.password.length === 0) {
            validationErrors.push("Password cannot be empty")
        }

        if(!this.state.firstName || this.state.firstName.length === 0) {
            validationErrors.push("First name cannot be empty")
        }

        if(!this.state.lastName || this.state.lastName.trim().length === 0) {
            validationErrors.push("Last name cannot be empty")
        }

        if(!this.state.email || this.state.email.trim().length === 0) {
            validationErrors.push("Email cannot be empty")
        } else if(!stringValidator.isValidEmail(this.state.email)) {
            validationErrors.push("Invalid email")
        }

        if(!this.state.gender) {
            validationErrors.push(`Please select your gender`)
        } else if(!["male", "female"].includes(this.state.gender)) {
            validationErrors.push(`Invalid gender`)
        }

        if(!this.state.passwordRetype || this.state.passwordRetype.length === 0) {
            validationErrors.push("Please re-enter your password")
        }
        else if(this.state.password !== this.state.passwordRetype) {
            validationErrors.push("Password does not match")
        }

        if(validationErrors.length > 0) {
            this.setState({
                "validationErrors": validationErrors
            })
            return
        }

        if(!this.state.userProfiles.me || !this.state.userProfiles.me._id || this.state.userProfiles.me._id.toString().trim().length === 0) {
            this.setState({
                "isCreatingUserProfile": true,
                "isSending": true,
                "network": Object.assign({}, this.state.network, {"errors": []})
            }, () => {
                this.props.dismissUserProfileErrors()
                this.props.registerUser({"username": this.state.username, "password": this.state.password, "name": this.state.name, "email": this.state.email, gender: this.state.gender})
            })
            return
        }

        if(!this.state.isCreatingUserProfile && this.state.userProfiles.me && this.state.userProfiles.me._id && this.state.userProfiles.me._id.toString().trim().length > 0) {
            this.setState({
                "isSending": true,
                "nextUrl": "/"
            })
    
            this.props.login({"username": this.state.username, "password": this.state.password})
        }

    }

    render() {
        return(
            <form 
                onSubmit={event => {
                    event.preventDefault()
                    this.handleSubmit()
                }} 
                method="post" 
                className="login100-form validate-form flex-sb flex-w"
                >
                <div className="p-t-31 p-b-9">
                    <span className="txt1">
                        Username <Required />
                    </span>
                </div>
                <div className="wrap-input100 validate-input">
                    <input 
                        className="input100" 
                        type="text" 
                        name="username"
                        disabled={this.state.isSending}
                        value={this.setState.username}
                        onChange={event => this.setState({"username": event.target.value})}
                        />
                    <span className="focus-input100"></span>
                </div>
            
                <div className="p-t-13 p-b-9">
                    <span className="txt1">
                        Password <Required />
                    </span>
                </div>
                <div className="wrap-input100 validate-input">
                    <input 
                        className="input100" 
                        type="password" 
                        name="password"
                        disabled={this.state.isSending}
                        value={this.state.password}
                        onChange={event => this.setState({"password": event.target.value})}
                        />
                    <span className="focus-input100"></span>
                </div>

                <div className="p-t-13 p-b-9">
                    <span className="txt1">
                        Re-type Password <Required />
                    </span>
                </div>
                <div className="wrap-input100 validate-input">
                    <input 
                        className="input100" 
                        type="password" 
                        name="passwordRetype"
                        disabled={this.state.isSending}
                        value={this.state.passwordRetype}
                        onChange={event => this.setState({"passwordRetype": event.target.value})}
                        />
                    <span className="focus-input100"></span>
                </div>

                <div className="p-t-13 p-b-9">
                    <span className="txt1">
                        First name <Required />
                    </span>
                </div>
                <div className="wrap-input100 validate-input">
                    <input 
                        className="input100" 
                        type="text" 
                        name="firstName"
                        disabled={this.state.isSending}
                        value={this.state.firstName}
                        onChange={event => {
                            this.setState({"firstName": event.target.value}, () => {
                                this.setState({"name": `${this.state.firstName} ${this.state.lastName}`})
                            })
                        }}
                        />
                    <span className="focus-input100"></span>
                </div>

                <div className="p-t-13 p-b-9">
                    <span className="txt1">
                        Last name <Required />
                    </span>
                </div>
                <div className="wrap-input100 validate-input">
                    <input 
                        className="input100" 
                        type="text" 
                        name="lastName" 
                        disabled={this.state.isSending}
                        value={this.state.lastName}
                        onChange={event => this.setState({"lastName": event.target.value}, () => {
                            this.setState({"name": `${this.state.firstName} ${this.state.lastName}`})
                        })}
                        />
                    <span className="focus-input100"></span>
                </div>

                <div className="p-t-13 p-b-9">
                    <span className="txt1">
                        Email <Required />
                    </span>
                </div>

                <div className="wrap-input100 validate-input">
                    <input 
                        className="input100"
                        name="email" 
                        disabled={this.state.isSending}
                        value={this.state.email}
                        onChange={event => this.setState({"email": event.target.value})}
                        />
                    <span className="focus-input100"></span>
                </div>

                <div className="p-t-13 p-b-9">
                    <span className="txt1">
                        Gender <Required />
                    </span>
                </div>

                <div className="wrap-input100 validate-input">
                    <select 
                        className="form-control input100"
                        name="gender" 
                        disabled={this.state.isSending}
                        value={this.state.gender}
                        onChange={event => this.setState({"gender": event.target.value})}
                    >
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                    </select>
                </div>

                {
                    this.state.validationErrors.length > 0 ? 
                        <div className="p-t-13 p-b-9">
                            <ul>
                                {this.state.validationErrors.map((error, i) => {
                                    return (
                                        <li key={i}><p className="text-danger">{error}</p></li>
                                    )
                                })}
                            </ul>
                        </div>
                    : null
                }

                {
                    this.state.network.errors.length > 0 ? 
                        <div className="p-t-13 p-b-9">
                            <ul>
                                {this.state.network.errors.map((error, i) => {
                                    return (
                                        <li key={i}><p className="text-danger">{error.error || error}</p></li>
                                    )
                                })}
                            </ul>
                        </div>
                     : null
                }

                <div className="container-login100-form-btn m-t-17">
                    <button className="login100-form-btn" disabled={this.state.isSending} >
                        {this.state.isSending ? <img src={loadingGif} height={20} width={20} /> : null}
                        &nbsp;
                        Register
                    </button>
                </div>

                <div className="w-full text-center p-t-55">
                    <span className="txt2">
                        Already have an account?
                    </span>
                    &nbsp;
                    <Link to="/auth" className="txt2 bo1">
                        Sign in now
                    </Link>
                </div>
            </form>
        )
    }
}

Register.propTypes = {
    "userProfiles": PropTypes.object.isRequired,
    "session": PropTypes.object.isRequired,
    "history": PropTypes.object.isRequired
}

export default Register