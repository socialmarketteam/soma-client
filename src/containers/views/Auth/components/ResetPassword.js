import React, {Component} from "react"
import {Link} from "react-router-dom"
import PropTypes from "prop-types"
import loadingGif from "../../../../assets/images/loading.gif"

class ResetPassword extends Component {

    constructor(props) {
        super(props)
        this.state = Object.assign({}, this.defaultState(props))
    }

    defaultState(props) {
        return {
            "session": props.session,
            "resetCode": props.match.params.code,
            "password": undefined,
            "passwordRetype": undefined,
            "success": false,
            "isSending": false,
            "validationErrors": [],
            "network": {
                "errors": []
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if(this.state.isSending && nextProps.session.account && nextProps.session.account._id && nextProps.session.account._id.toString().trim().length > 0) {
            this.setState({
                "session": nextProps.session,
                "isSending": false,
                "success": true,
                "network": {
                    "errors": []
                }
            })
        }
        if(nextProps.session.network.errors.length > 0) {
            this.setState({
                "isSending": false,
                "network": {
                    "errors": nextProps.session.network.errors
                }
            })
        }
    }

    handleSubmit() {
        let validationErrors = []

        if(!this.state.password || this.state.password.length === 0) {
            validationErrors.push("Password cannot be empty")
        }

        if(!this.state.passwordRetype || this.state.passwordRetype.length === 0) {
            validationErrors.push("Please retype your password")
        }
        else if(this.state.password !== this.state.passwordRetype) {
            validationErrors.push("Password does not match")
        }

        if(validationErrors.length > 0) {
            this.setState({
                "validationErrors": validationErrors
            })
            return
        }

        this.setState({
            "isSending": true,
            "success": false,
            "network": {
                "errors": []
            }
        }, () => {
            this.props.resetPassword(this.state.password, this.state.resetCode)
        })
    }

    render() {
        return (
            <form onSubmit={(event) => { 
                event.preventDefault()
                this.handleSubmit()
            }} 
            method="post" 
            className="login100-form validate-form flex-sb flex-w"
            >
                <div className="p-t-13 p-b-9">
                    <p className="txt1">
                        New password
                    </p>
                </div>
                <div className="wrap-input100 validate-input">
                    <input
                        className="input100" 
                        name="password"
                        type="password"
                        value={this.state.password}
                        disabled={this.state.isSending}
                        onChange={(event) => {
                            this.setState({
                                "password": event.target.value
                            })
                        }}
                    />
                    <span className="focus-input100"></span>
                </div>

                <div className="p-t-13 p-b-9">
                    <p className="txt1">
                        Re-type password
                    </p>
                </div>
                <div className="wrap-input100 validate-input">
                    <input
                        className="input100" 
                        name="passwordRetype"
                        type="password"
                        value={this.state.passwordRetype}
                        disabled={this.state.isSending}
                        onChange={(event) => {
                            this.setState({
                                "passwordRetype": event.target.value
                            })
                        }}
                    />
                    <span className="focus-input100"></span>
                </div>

                {
                    this.state.validationErrors.length > 0 ? 
                        <div className="p-t-13 p-b-9">
                            <ul>
                                {this.state.validationErrors.map((error, i) => {
                                    return (
                                        <li key={i}><p className="text-danger">{error}</p></li>
                                    )
                                })}
                            </ul>
                        </div>
                    : null
                }

                {
                    this.state.network.errors.length > 0 ? 
                        <div className="p-t-13 p-b-9">
                            <ul>
                                {this.state.network.errors.map((error, i) => {
                                    return (
                                        <li key={i}><p className="text-danger">{error.error || error}</p></li>
                                    )
                                })}
                            </ul>
                        </div>
                    : null
                }

                {
                    this.state.success ? 
                        <div className="p-t-13 p-b-9">
                            <p className="text-info">Your password has been updated. Please <Link to="/auth">sign in</Link> using your account</p>
                        </div>
                    : null
                }

                <div className="container-login100-form-btn m-t-17">
                    <button 
                        className="login100-form-btn"
                        disabled={this.state.isSending}
                    >
                            {this.state.isSending ? <img src={loadingGif} height={20} width={20} /> : null}
                            &nbsp;
                            {this.state.isSending ? "Updating your password" : "Update my password"}
                    </button>
                </div>
            </form>
        )
    }
}

ResetPassword.propTypes = {
    "session": PropTypes.object.isRequired,
    "history": PropTypes.object.isRequired
}

export default ResetPassword