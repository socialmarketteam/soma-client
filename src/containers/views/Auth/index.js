import { connect } from "react-redux"
import UserSession from "./UserSession"

import sessionActions from "../../../data/UserSession/actions"
import userProfileActions from "../../../data/UserProfile/actions"

const mapStateToProps = (state) => {
    return {
        "session": state.session,
        "userProfiles": state.userProfiles
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        "login": (account) => {
            dispatch(sessionActions.loginAction(account))
        },
        "registerUser": (userProfile) => {
            dispatch(userProfileActions.userProfileWillCreateAction(userProfile))
        },
        "sendResetPasswordEmail": (email) => {
            dispatch(sessionActions.sendResetPasswordEmailAction(email))
        },
        "resetPassword": (newPassword, code) => {
            dispatch(sessionActions.resetPassword(newPassword, code))
        },
        "dismissUserProfileErrors": () => {
            dispatch(userProfileActions.clearUserProfileErrors())
        },
        "dismissUserSessionErrors": () => {
            dispatch(sessionActions.clearUserSessionErrors())
        },
    }
}

const UserSessionContainer = connect(mapStateToProps, mapDispatchToProps)(UserSession)

export default UserSessionContainer
