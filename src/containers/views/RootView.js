import React, {Component} from "react"
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import _ from "lodash"
// views
import NewsFeedContainer from "./client/NewsFeed"
import ProfileContainer from "./Client/Profile"
import SearchModule from "./Client/Search"

import {subscribeToFriendRequest, socket} from "../../socket"
import Firebase from "../../firebase"
import Sound from "react-sound"

import newMessageSound from "../../assets/sounds/messagetone.mp3"
import notificationSound from "../../assets/sounds/notification_sound.mp3"
import SinglePostModule from "./Client/SinglePost";

const NotFound = () => (
    <h1>404.. This page is not found!</h1>)

class RootView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            newMessageSoundPlayStatus: Sound.status.STOPPED,
            newMessageNotified: false,
            newNotiSoundPlayStatus: Sound.status.STOPPED
        }
    }

    componentWillMount() {
        if(!this.props.session || !this.props.session.sessionToken) {
            return this.props.history.push("/auth");
        }
        subscribeToFriendRequest(this.props.session.user._id, {
            onUpdated: this.props.updateFriendRequests
        })
        if(!socket.connected) {
            socket.connect();
            socket.emit('identify', this.props.session.sessionToken)
        } else {
            socket.emit('identify', this.props.session.sessionToken)
        }
    }

    componentWillReceiveProps(nextProps) {
        if(!nextProps.session.sessionToken) {
            return this.props.history.push("/auth");
        }
    }
    componentDidMount() {
        if(this.props.session && this.props.session.user && this.props.session.user._id) {
            Firebase.firestore.notification.subscribeUserNotifications(this.props.session.user._id, this.notificationPushed.bind(this));
        }
    }
    notificationPushed(doc) {
        if(!doc) return;
        let {id, messages, news} = doc;
        var unseenNotifications = {
            messages: messages.filter(message => message.from != this.props.session.user._id && !message.isSeen),
            news: _.orderBy(news, 'createdAt', "desc")
        }
        messages = messages ? messages.filter(message => message.from != this.props.session.user._id && !message.isNotified) : []
        messages = _.uniqBy(messages, 'conversationId');
        if(messages.length) {
            this.playNewMessageSound();
            messages.forEach(message => {
                let existingBox = this.props.chat.chatBoxes.find(box => box.id == message.conversationId);
                if(!existingBox) {
                    this.props.addChatbox(message.conversationId)
                }
            })
        }
        news = news ? news.filter(n => !n.isNotified) : []
        if(news.length) {
            this.playNotificationSound()
        }
        this.props.updateNotification(unseenNotifications)
    }

    playNotificationSound() {
        this.setState({
            newNotiSoundPlayStatus: Sound.status.PLAYING
        })
    }

    playNewMessageSound() {
        this.setState({
            newMessageSoundPlayStatus: Sound.status.PLAYING
        })
    }

    onNotificationSoundFinished(stateSoundPropName, type) {
        return () => {
            this.setState({
                [stateSoundPropName]: Sound.status.STOPPED
            }, () => {
                switch(type) {
                    case "message":
                        Firebase.firestore.notification.updateMessagesAsNotified()
                    break;
                    case "news":
                        Firebase.firestore.notification.updateNewsAsNotified()
                    break;
                    default: break;
                }
            })
        }
    }
    
    render() {
        return (
            <div>
                <Sound 
                    url={newMessageSound}
                    playStatus={this.state.newMessageSoundPlayStatus}
                    playFromPosition={0}
                    onFinishedPlaying={this.onNotificationSoundFinished("newMessageSoundPlayStatus", "message").bind(this)}
                />
                <Sound
                    url={notificationSound}
                    playStatus={this.state.newNotiSoundPlayStatus}
                    playFromPosition={0}
                    onFinishedPlaying={this.onNotificationSoundFinished("newNotiSoundPlayStatus", "news").bind(this)}
                />
                <Router>
                    <Switch>
                        <Route exact path="/" render={routerProps => {
                            return <NewsFeedContainer {...this.props} {...routerProps} />
                        }} />
                        <Route path="/user/:_id" render={routerProps => {
                            return <ProfileContainer {...this.props} {...routerProps} />
                        }} />
                        <Route path="/search" render={routerProps => {
                            return <SearchModule {...this.props} {...routerProps} />
                        }} />
                        <Route path="/post/:_id" render={routerProps => {
                            return <SinglePostModule {...this.props} {...routerProps} />
                        }} />
                        <Route path="/404" component={NotFound} />
                        <Route path="*" component={NotFound} />
                    </Switch>
                </Router>
            </div>
        )
    }
}

export default RootView