import React, { Component } from 'react'
import "../../../assets/css/layouts/client.css"
import ChatBar from "./components/ChatBar"
import LeftSidebar from "./components/LeftSidebar"
import NavBar from "../NavBar/index"
import { connect } from "react-redux"
import chatActions from "../../../data/Chat/actions"
import postActions from "../../../data/Post/actions"
import ChatContainer from "./components/ChatContainer"
import Alert from '../../../presentations/Alert';
import alertActions from "../../../data/Alert/actions"

class Client extends Component {
    constructor(props) {
        super(props)
        this.state = {
            alerts: []
        }
    }

    pushAlert(alert) {
        this.setState({
            alerts: [...this.state.alerts, alert]
        })
    }
    clearAlert(index) {
        this.setState({
            alerts: this.state.alerts.filter((alert, i) => i != index)
        })
    }

    render() {
        return (
            <div>
                <NavBar {...this.props} />

                <ChatBar {...this.props} />

                <div className="mainContainer">
                    <Alert {...this.props} />
                    <div className="container">
                        <div className="row" >
                            {
                                this.props.enableLeftSideBar
                                    ?
                                    <div style={{width: "100%"}}>
                                        {/* <div className="left-sidebar">
                                            <LeftSidebar />
                                        </div> */}
                                        <div className="mainContent">
                                            {this.props.children}
                                        </div>
                                    </div>
                                    : this.props.children
                            }
                        </div>
                    </div>
                </div>

                <ChatContainer {...this.props}/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return state
}

const mapDispatchToProps = (dispatch) => {
    return {
        "createChatBox": ({users, chatName, imagePath}) => {
            dispatch(chatActions.addChatBox({users, chatName, imagePath}))
        },
        "closeChatBox": (uuid) => {
            dispatch(chatActions.closeChatBox(uuid))
        },
        "sendMessage": (conversationId, message) => {
            dispatch(chatActions.sendMessage(conversationId, message))
        },
        "loadConversation": (conversation) => {
            dispatch(chatActions.loadConversation(conversation))
        },
        "subscribeToConversation": (conversation) => {
            dispatch(chatActions.subscribeToConversation(conversation))
        },
        "createPost": (post, creator) => {
            dispatch(postActions.createPost(post, creator))
        },
        "pushAlert": (alert) => {
            dispatch(alertActions.pushAlert(alert))
        },
        "removeAlert": (alertUuid) => {
            dispatch(alertActions.removeAlert(alertUuid))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Client)