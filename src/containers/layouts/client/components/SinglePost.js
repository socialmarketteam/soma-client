import React from "react"
import moment from "moment"
import PostComment from "./PostComment";
import CommentTool from "./CommentTool";
import postActions from "../../../../data/Post/actions"
import {connect} from "react-redux"
import uuid from "uuid/v4"

const maleImg = require("../../../../assets/images/user_male.jpg")
const femaleImg = require("../../../../assets/images/user_female.jpg")

class SinglePost extends React.Component {
    
    likePost() {
        this.props.likePost(this.props.post._id, this.props.session.user, this.props.post.creator._id);
    }
    dislikePost() {
        this.props.dislikePost(this.props.post._id);
    }

    render() {
        const creator = this.props.post.creator || {};
        const comments = this.props.post.comments || [];
        const likes = this.props.post.likes || [];
        const currentUser = this.props.session.user || {};
        const avatar = creator.avatar_path || (creator.gender == "female" ? femaleImg : maleImg)
        const liked = this.props.post.likes.find(like => like == currentUser._id || like._id == currentUser._id)
        return (
            <div className={`card mt-1 ${this.props.containerClass || ""}`}>
                <div className="card-body pt-2">
                    <div className="row pl-2 pr-2">
                        <div className="post-creator-img-container">
                            <img src={avatar} className="round-img" width="100%" height="100%" />
                        </div>
                        <div className="post-info">
                            <div className="row">
                                <a onClick={event => {
                                    event.preventDefault();
                                    this.props.history.push(`/user/${creator._id}`)
                                }} className="clickable hover-underline"><b>{creator.name}</b></a>
                            </div>
                            <div className="row">
                                <p className="text-muted">{this.props.post.created_at ? moment(this.props.post.created_at).local().format("YYYY-MM-DD hh:mm A") : ""}</p>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-2 pl-2 pr-2">
                        <p className="wrap-content post-content" style={{maxWidth: "100%"}}>{this.props.post.post_content}</p>
                    </div>
                    <div className="row mt-2 pl-2 pr-2 justify-content-between">
                        {likes.length ? 
                            <div>
                                <p>{likes.length} {likes.length > 1 ? "likes" : "like"}</p>
                            </div>
                        : <div></div>}
                        {comments.length ? 
                            <div className="justify-content-end">
                                <p>{comments.length} {comments.length > 1 ? "comments" : "comment"}</p>
                            </div>
                        : null}
                    </div>
                    <div className="row mt-2 pl-2 pr-2 justify-content-between react-tool">
                        <button 
                            type="button" 
                            className="btn btn-default react-button"
                            onClick={event => {
                                event.preventDefault();
                                if(!liked) {
                                    this.likePost()
                                } else {
                                    this.dislikePost()
                                }
                            }}
                        ><i style={liked ? {color: "blue"} : {}} className={`fa${liked ? 's' : 'r'} fa-thumbs-up`}></i>&nbsp;&nbsp;{liked ? 'Liked' : 'Like'}
                        </button>
                        <button type="button" className="btn btn-default react-button" onClick={event => {
                                event.preventDefault();
                                // let commentContainer = document.getElementById(`commentContainer_${this.props.post._id}`);
                                // if(commentContainer) {
                                //     if(commentContainer.classList.contains('d-none')) {
                                //         commentContainer.classList.remove('d-none')
                                //     } else {
                                //         commentContainer.classList.add('d-none')
                                //     }
                                // }
                            }}><i className="far fa-comment-alt"></i>&nbsp;&nbsp;Comment</button>
                        {/* <button type="button" className="btn btn-default react-button"><i className="far fa-share-square"></i>&nbsp;&nbsp;Share</button> */}
                    </div>
                    <div className="mt-2" id={`commentContainer_${this.props.post._id}`}>
                        {this.props.post.comments.map(comment => {
                            return (
                                <PostComment {...this.props} comment={comment} key={uuid()} />
                            )
                        })}
                        <CommentTool {...this.props} />
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return state
}

const mapDispatchToProps = (dispatch) => {
    return {
        "likePost": (postId, likeUserName, creatorId) => {
            dispatch(postActions.likePost(postId,likeUserName, creatorId))
        },
        "dislikePost": (postId) => {
            dispatch(postActions.dislikePost(postId))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SinglePost)