import React, {Component} from "react"
import moment from "moment"
import uuid from "uuid/v4"

import user_maleImg from "../../../../assets/images/user_male.jpg"
import user_femaleImg from "../../../../assets/images/user_female.jpg"
import Firebase from "../../../../firebase";

const defaultState = (props) => {
    return {
        messageInput: "",
        messages: props.chatbox.messages || [],
        isLoading: false
    }
}

class Chatbox extends Component {

    constructor(props) {
        super(props)
        this.state = defaultState(props)
    }

    componentWillMount() {
        this.setState({
            isLoading: true
        })

        this.props.loadConversation(this.props.chatbox);
    }

    componentDidMount() {
        var input = document.getElementById('messageInput');
        if(input) {
            input.onfocus = () => {
                if(this.props.chatbox.id) Firebase.firestore.notification.updateMessagesAsSeen(this.props.chatbox.id)
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if(this.state.isLoading && !nextProps.chatbox.isLoading) {
            this.setState({
                isLoading: false
            })
            if(nextProps.chatbox) this.props.subscribeToConversation(nextProps.chatbox)
            this.scrollToBottom(this.props)
        }

        if(this.props.chatbox.messages.length != nextProps.chatbox.messages.length) {
            this.scrollToBottom(nextProps)
        }
    }

    scrollToBottom(props) {
        setTimeout(() => {
            var chatBoxBody = document.getElementById(`chatBoxBody${props.chatbox.id}`);
            var lastMessage;
            if(this.lastMessageId) lastMessage = document.getElementById(this.lastMessageId)
            if(chatBoxBody) {
                let lastMessageHeight = lastMessage ? lastMessage.clientHeight : 0;
                chatBoxBody.scrollTop = chatBoxBody.scrollHeight + lastMessageHeight;
            }
        }, 500)
    }

    sendMessage(event) {
        if(event) event.preventDefault();
        var messageText = this.state.messageInput;
        if(!messageText) return;
        var message = {
            uuid: uuid(),
            message: messageText,
            from: this.props.session.user._id,
            sent_at: moment.utc().toDate()
        }
        this.setState({
            messageInput: "",
            messages: [...this.state.messages, message]
        })

        this.props.sendMessage(this.props.chatbox.id, message)
    }

    handleMessageInputChanged(event) {
        if(event) event.preventDefault();

        this.setState({
            messageInput: event.target.value
        })
    }

    handleMessageInputKeyPressed(event) {
        if(!event) return;
        if(event.key === "Enter") {
            this.sendMessage();
        }
    }

    render() {
        let chatbox = this.props.chatbox
        chatbox.name = (chatbox.users.find(user => user._id != this.props.session.user._id) || {}).name;
        return (
            <div className={`chatbox ${chatbox.minimized ? `minimized` : ``}`}>
                <div className="chatbox-header" onClick={event => {
                    event.preventDefault();
                    chatbox.minimized = !chatbox.minimized;
                    this.setState(this.state || {})
                }}>
                    <div className="col-md-12">
                        <div className="row">
                            <div className="col-md-9">
                                {/* <img src={chatbox.imagePath} className="round-img mr-2" width="22" height="22" /> */}
                                <a href="javascript:void">{chatbox.name}</a>
                            </div>
                            <div className="col-md-3">
                                <span 
                                    className="float-right chatbox-close-btn"
                                    onClick={event => {
                                        event.preventDefault();
                                        this.props.closeChatBox(chatbox.uuid)
                                    }}
                                >&times;</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div id={`chatBoxBody${chatbox.id}`} className="chatbox-body">
       
                        <div className="p-2">
                            <div className="chatbox-message-container">
                                {chatbox.messages.map((message, index) => {
                                    var user = this.props.chatbox.users.find(user => user._id == message.from) || {}
                                    var isFromCurrentUser = user._id == this.props.session.user._id;
                                    var imgPath = user.avatar_path
                                                && user.avatar_path.trim().length > 0
                                                ? user.avatar_path
                                                : (user.gender
                                                    && user.gender === "male"
                                                    ? user_maleImg
                                                    : user_femaleImg);
                                    var elementId = `conversation_${this.props.chatbox.id}_message_${message.uuid}`;
                                    if(index == chatbox.messages.length - 1) {
                                        this.lastMessageId = elementId
                                    }
                                    var sentAt = moment.utc(message.sent_at.seconds * 1000).local().format('YYYY/MM/DD - HH:mm');
                                    return (
                                            <div id={elementId} key={`conversation_${this.props.chatbox.id}_message_${message.uuid}`} className={`row single-message ${isFromCurrentUser ? "right" : "left"} pr-2 pl-2 mb-1`}>
                                                <div className="col">
                                                    {isFromCurrentUser ? 
                                                        <div title={sentAt} className="chatbox-message-user-img float-right clickable" style={{backgroundImage: `url('${imgPath}')`}} onClick={event => {
                                                            event.preventDefault();
                                                            location.assign(`/user/${user._id}`)
                                                        }}></div>
                                                        : null}
                                                    {!isFromCurrentUser ? 
                                                        <div title={sentAt} className="chatbox-message-user-img float-left clickable" style={{backgroundImage: `url('${imgPath}')`}} onClick={event => {
                                                            event.preventDefault();
                                                            location.assign(`/user/${user._id}`)
                                                        }}></div>
                                                        : null}
                                                    <div className={`chatbox-message ${isFromCurrentUser ? "right" : "left"} mr-1 ml-1 p-2 float-${isFromCurrentUser ? "right" : "left"}`}>
                                                        <p>{message.message}</p>
                                                    </div>
                                                </div>
                                            </div>
                                    )
                                })}
                            </div>
                        </div>
                    
                </div>
                <div className="chatbox-footer">
                    <div className="col-md-12">
                        <div className="row">
                            <div className="d-inline message-input">
                                <input 
                                    className="form-control" 
                                    type="text" 
                                    id={`messageInput`}
                                    placeholder="Type a message..." 
                                    value={this.state.messageInput} 
                                    onChange={this.handleMessageInputChanged.bind(this)} 
                                    onKeyPress={this.handleMessageInputKeyPressed.bind(this)}                                   
                                />
                            </div>
                            <button 
                                className="btn btn-link d-inline send-btn"
                                onClick={this.sendMessage.bind(this)}
                            >Send</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Chatbox