import React from "react"
import moment from "moment";
const maleImg = require("../../../../assets/images/user_male.jpg")
const femaleImg = require("../../../../assets/images/user_female.jpg")

export default class PostComment extends React.Component {
    render() {
        const comment = this.props.comment || {};
        const creator = comment.creator || {};
        const avatar = creator.avatar_path || (creator.gender == "female" ? femaleImg : maleImg)
        return (
            <div className={`row col max-width mb-2 ${this.props.containerClass || ""}`}>
                <div className="comment-creator-img-container">
                    <img src={avatar} className="round-img" width="100%" height="100%" />
                </div>
                <div>
                    <div className="comment-content-container">
                        <p><a className="clickable hover-underline" onClick={event=> {
                            event.preventDefault();
                            if(creator._id) this.props.history.push(`/user/${creator._id}`)
                        }}><b>{creator.name}</b></a> <span className="post-content">{comment.content}</span></p>
                    </div>
                    <small style={{marginLeft: "15px"}}>{moment(comment.created_at).local().format('YYYY-MM-DD hh:mm A')}</small>
                </div>
            </div>
        )
    }
}