import React, {Component} from "react"
import querystring from "query-string"

class SearchBar extends Component {
    constructor(props) {
        super(props)
        var query = querystring.parse(props.location.search, {ignoreQueryPrefix: true})
        this.state = {
            searchText: query.s || ""
        }
    }

    handleSubmit(event) {
        event.preventDefault();
        if(this.state.searchText) location.assign(`/search?s=${this.state.searchText}`)
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit.bind(this)} className={this.props.className}>
                <input type="text" placeholder="Search..." value={this.state.searchText} className="form-control" onChange={(event) => {
                    this.setState({
                        searchText: event.target.value
                    })
                }} />
            </form>
        )
    }
}

export default SearchBar