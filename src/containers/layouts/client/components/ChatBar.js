import React, {Component} from 'react'
import "../../../../assets/css/components/chatbar.css"

import user_maleImg from "../../../../assets/images/user_male.jpg"
import user_femaleImg from "../../../../assets/images/user_female.jpg"

class ChatBar extends Component {
    render() {
        return (
            <div className="chatbar">
                <div className="chatbar-element">
                    <div className="col-md-12">
                        <div className="pt-2">
                            <strong>Contacts</strong>
                        </div>
                    </div>
                    <div className="col-md-12">
                        {this.props.session.user ?
                            (this.props.session.user.friends || []).filter(friend => friend._id).map(friend => {
                                var imgPath = friend.avatar_path
                                                && friend.avatar_path.trim().length > 0
                                                ? friend.avatar_path
                                                : (friend.gender
                                                    && friend.gender === "male"
                                                    ? user_maleImg
                                                    : user_femaleImg);
                                return (
                                    <div key={`userFriends_${friend._id}`} 
                                        className="row pt-2 pb-2 contact-single"
                                        onClick={(event) => {
                                            event.preventDefault();
                                            this.props.createChatBox({
                                                users: [this.props.session.user, friend],
                                                chatName: friend.name,
                                                // imagePath: imgPath
                                            })
                                        }}
                                    >
                                        <div className="col-md-12">
                                            <img className="round-img"
                                                src={imgPath}
                                                width={32}
                                                height={32} />
                                            <button className="btn btn-link">{friend.name}</button>
                                        </div>
                                    </div>
                                )
                                    
                            })
                        : null}
                    </div>
                </div>
            </div>
        )
    }
}

export default ChatBar