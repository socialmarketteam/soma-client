import React from "react"
import {connect} from "react-redux"
import postActions from "../../../../data/Post/actions"

class CommentTool extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            content: "",
            isSending: false
        }
    }
    componentWillReceiveProps(nextProps) {
        if(this.state.isSending && !nextProps.posts.isUpdating) {
            this.setState({
                isSending: false,
                content: nextProps.posts.error ? this.state.content : ""
            })
        }
    }
    handleSubmit(event) {
        event.preventDefault();
        if(!this.state.content) return;
        this.setState({
            isSending: true
        })
        this.props.comment(this.props.post, {
            content: this.state.content
        }, this.props.session.user)
    }
    render() {
        return (
            <form className="form-inline max-width mt-2" onSubmit={this.handleSubmit.bind(this)}>
                <input type="text" className="form-control max-width" placeholder="Write a comment..." value={this.state.content} onChange={event => {
                    this.setState({
                        content: event.target.value
                    })
                }} />
            </form>
        )
    }
}

const mapStateToProps = (state) => {
    return state
}

const mapDispatchToProps = (dispatch) => {
    return {
        comment: (post, comment, user) => {
            dispatch(postActions.comment(post, comment, user))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CommentTool)