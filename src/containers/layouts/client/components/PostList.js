import React from "react"
import SinglePost from "./SinglePost";
import uuid from "uuid/v4"

class PostList extends React.Component {
    render() {
        return (
            <div className="post-list">
                {this.props.posts.data.map(post => {
                    return <SinglePost key={uuid()} {...this.props} post={post} />
                })}
            </div>
        )
    }
}

export default PostList