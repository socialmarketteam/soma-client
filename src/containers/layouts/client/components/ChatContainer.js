import React, {Component} from "react"
import Chatbox from "./Chatbox"
import "../../../../assets/css/components/chatcontainer.css"
import uuid from "uuid/v4"

class ChatContainer extends Component {
    render() {
        return (
            <div className="chat-container">
            {this.props.chat.chatBoxes.map((chatbox,index) => {
                chatbox.minimized = chatbox.minimized === undefined ? false : chatbox.minimized;
                return (
                    <Chatbox key={`chatbox_${index}`} {...this.props} chatbox={chatbox} />
                )
            })}
            </div>
        )
    }
}

export default ChatContainer