import React, {Component} from "react"
import PropTypes from "prop-types"
import {connect} from "react-redux"
import postActions from "../../../../data/Post/actions"

class PostComposer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            content: "",
            isSending: false
        }
    }
    componentWillReceiveProps(nextProps) {
        if(this.state.isSending && !nextProps.posts.isCreating) {
            this.setState({
                isSending: false,
                content: ""
            })
        }
    }
    handleSubmitPost() {
        if(this.state.content) {
            this.setState({
                isSending: true
            })
            const post = {
                creator: this.props.creator._id,
                post_content: this.state.content
            }
            this.props.createPost(post, this.props.creator)
        }
    }
    render() {
        return (
            <div className={`${this.props.containerClassName} card`}>
                <div className="card-body">
                    <div className="row">
                        <textarea placeholder="What's in your mind?" value={this.state.content} className="col-12 textarea-no-resizable border-bottom-muted" rows={3} onChange={event => {
                            if(this.state.isSending) return;
                            this.setState({
                                content: event.target.value
                            })
                        }} />
                    </div>
                    <div className="row justify-content-between pt-2">
                        <div></div>
                        <button className="btn btn-primary justify-content-end pt-0 pb-0" disabled={this.state.isSending} onClick={event => {
                            event.preventDefault();
                            this.handleSubmitPost()
                        }}>Post</button>
                    </div>
                </div>
            </div>
        )
    }
}

PostComposer.propTypes = {
    containerClassName: PropTypes.string,
    creator: PropTypes.any
}

const mapStateToProps = (state) => {
    return state
}

const mapDispatchToProps = (dispatch) => {
    return {
        "createPost": (post,creator) => {
            dispatch(postActions.createPost(post, creator))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PostComposer)