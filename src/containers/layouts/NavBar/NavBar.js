import React, { Component } from 'react';
import logo from '../../../assets/images/logo.png';
import '../../../assets/css/components/navbar.css';
import { Link } from 'react-router-dom';
import Firebase from '../../../firebase';
import uuid from 'uuid/v4';
import _ from 'lodash';

import user_maleImg from '../../../assets/images/user_male.jpg';
import user_femaleImg from '../../../assets/images/user_female.jpg';
import friend_request_icon from '../../../assets/images/friend_request_icon.png';
import messenger_icon from '../../../assets/images/messenger_icon.png';
import notification_icon from '../../../assets/images/notification_icon.png';

import SearchBar from '../client/components/SearchBar';
import NotificationDot from '../../../presentations/NotificationDot/NotificationDot';

const avatar = {
	male: user_maleImg,
	female: user_femaleImg
};

class NavBar extends Component {
	constructor(props) {
		super(props);
		this.state = Object.assign({}, this.defaultState(props));
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			session: nextProps.session
		});
	}

	shouldComponentUpdate(nextProps) {
		return nextProps.session.user != null;
	}

	componentDidMount() {
		document.onclick = (event) => {
			var dropdownBtn = document.getElementById('nav.settingDropdown');
			var dropdownCarret = document.getElementById('nav.settingDropdownCarret');
			var dropdownMenu = document.getElementById('nav.settingDropdownMenu');
			if (event.target != dropdownBtn && event.target != dropdownCarret && event.target != dropdownMenu) {
				this.setState({
					showSettingDropdown: false
				});
			}

			dropdownBtn = document.getElementById('nav.friendRequestsDropdown');
			dropdownCarret = document.getElementById('nav.friendRequestsDropdownIcon');
			dropdownMenu = document.getElementById('nav.friendRequestsDropdownMenu');
			let dropdownIconImg = document.getElementById('friendRequestsDropdownIconImg');
			if (
				event.target != dropdownBtn &&
				event.target != dropdownCarret &&
				event.target != dropdownMenu &&
				event.target != dropdownIconImg
			) {
				this.setState({
					showFriendRequestDropdown: false
				});
			}

			dropdownBtn = document.getElementById('nav.messageDropdown');
			dropdownCarret = document.getElementById('nav.messagesDropdownIcon');
			dropdownMenu = document.getElementById('nav.messagesDropdownMenu');
			dropdownIconImg = document.getElementById('messagesDropdownIconImg');
			if (
				event.target != dropdownBtn &&
				event.target != dropdownCarret &&
				event.target != dropdownMenu &&
				event.target != dropdownIconImg
			) {
				this.setState({
					showMessageDropdown: false
				});
			}

			dropdownBtn = document.getElementById('nav.newsDropdown');
			dropdownCarret = document.getElementById('nav.newsDropdownIcon');
			dropdownMenu = document.getElementById('navnewsDropdownMenu');
			dropdownIconImg = document.getElementById('newsDropdownIconImg');
			if (
				event.target != dropdownBtn &&
				event.target != dropdownCarret &&
				event.target != dropdownMenu &&
				event.target != dropdownIconImg
			) {
				this.setState({
					showNotiDropdown: false
				});
			}
		};

		this.props.refreshSession();
	}

	loadConversations() {
		if (!this.state.session.user._id || !this.state.showMessageDropdown) return;
		this.setState({
			isLoadingMessages: true
		});
		Firebase.firestore.conversation.loadConversations(
			'users',
			'array-contains',
			this.state.session.user._id,
			(err, messages) => {
				this.setState({
					isLoadingMessages: false
				});
				if (err) return;

				this.setState({
					conversations: messages || this.state.conversations
				});
			}
		);
	}

	defaultState(props) {
		return {
			session: props.session,
			showSettingDropdown: false,
			showFriendRequestDropdown: false,
			showMessageDropdown: false,
			showNotiDropdown: false,
			isLoadingMessages: false,
			conversations: []
		};
	}

	updateMessageNotificationAsSeen(conversationId) {
		if (conversationId) Firebase.firestore.notification.updateMessagesAsSeen(conversationId);
	}

	render() {
		this.state.session.user = this.state.session.user || {};
		return (
			<nav className="navbar fixed-top">
				<div className="container">
					<div className="navbar-header">
						<Link to="/" className="navbar-brand">
							<img className="logo" src={logo} />
						</Link>
					</div>
					<SearchBar className="col-md-6" {...this.props} />
					<div className="col-md">
						<ul className="nav">
							<li
								onClick={() => {
									this.props.history.push(`/user/${this.state.session.user._id}`);
								}}
							>
								<a href={`#`}>
									<img
										className="avatar"
										src={
											this.state.session.user &&
											this.state.session.user.avatar_path &&
											this.state.session.user.avatar_path.trim().length > 0 ? (
												this.state.session.user.avatar_path
											) : this.state.session.user &&
											this.state.session.user.gender &&
											this.state.session.user.gender === 'male' ? (
												user_maleImg
											) : (
												user_femaleImg
											)
										}
										width={25}
										height={25}
									/>{' '}
									&nbsp;
									{this.state.session.user.name}
								</a>
							</li>
							<li
								onClick={() => {
									location.assign('/');
								}}
							>
								<Link to="#">Home</Link>
							</li>
							<li
								className="nav-dropdown-item"
								id="nav.friendRequestsDropdown"
								onClick={() => {
									this.setState({ showFriendRequestDropdown: !this.state.showFriendRequestDropdown });
								}}
							>
								<div>
									<span id="nav.friendRequestsDropdownIcon">
										<img
											id="friendRequestsDropdownIconImg"
											className="dropdown-icon"
											width="100%"
											src={friend_request_icon}
										/>
									</span>
									<NotificationDot
										notificationCount={this.props.session.user.friend_requests.length}
									/>
									{this.state.showFriendRequestDropdown ? (
										<div
											id="nav.friendRequestsDropdownMenu"
											className="nav-dropdown-menu friend-request-dropdown card"
										>
											<div className="card-header">
												<p>Friend Requests</p>
											</div>
											<div className="card-body">
												{this.props.session.user.friend_requests.map((request) => {
													return (
														<div className="row mb-3" key={`friend_request_${request._id}`}>
															<div className="col-md-2">
																<img
																	src={request.avatar_path || user_maleImg}
																	width="100%"
																/>
															</div>
															<div className="col-xs-6">
																<button
																	className="btn btn-link user-name"
																	onClick={(event) => {
																		this.props.history.push(`/user/${request._id}`);
																	}}
																>
																	{request.name}
																</button>
															</div>
														</div>
													);
												})}
											</div>
										</div>
									) : null}
								</div>
							</li>
							<li
								className="nav-dropdown-item"
								id="nav.messageDropdown"
								onClick={() => {
									this.setState(
										{ showMessageDropdown: !this.state.showMessageDropdown },
										this.loadConversations.bind(this)
									);
								}}
							>
								<div>
									<span id="nav.messagesDropdownIcon">
										<img
											id="messagesDropdownIconImg"
											className="dropdown-icon"
											width="100%"
											src={messenger_icon}
										/>
									</span>
									<NotificationDot
										notificationCount={
											_.uniqBy(this.props.notifications.messages, 'conversationId').length
										}
									/>
									{this.state.showMessageDropdown ? (
										<div
											id="nav.messagesDropdownMenu"
											className="nav-dropdown-menu friend-request-dropdown card"
										>
											<div className="card-header">
												<p>Messages</p>
											</div>
											<div className="card-body p-0">
												{this.state.isLoadingMessages ? (
													<div style={{ width: '100%', height: '50px' }}>
														<div
															className="loader"
															style={{ backgroundColor: 'transparent' }}
														/>
													</div>
												) : (
													this.state.conversations.map((conversation) => {
														var unSeenMessages =
															this.props.notifications.messages.filter(
																(message) =>
																	message.conversationId == conversation.id &&
																	!message.isSeen
															) || [];
														var lastMessage =
															(conversation.messages || [])[
																conversation.messages.length - 1
															] || {};
														return (
															<div
																className={`row mb-3 nav-message-single${unSeenMessages.length
																	? ' unseen'
																	: ''}`}
																key={uuid()}
																onClick={(event) => {
																	event.preventDefault();
																	this.props.createChatbox({
																		users: conversation.users.map((_id) => {
																			return {
																				_id
																			};
																		}),
																		chatName: conversation.name
																	});
																	this.updateMessageNotificationAsSeen(
																		conversation.id
																	);
																}}
															>
																<div className="col-12">
																	<strong>{conversation.name}</strong>
																</div>
																<div className="col-12">
																	<p className="pb-1">{lastMessage.message}</p>
																</div>
															</div>
														);
													})
												)}
											</div>
										</div>
									) : null}
								</div>
							</li>
							<li
								className="nav-dropdown-item"
								id="nav.newsDropdown"
								onClick={() => {
									this.setState({ showNotiDropdown: !this.state.showNotiDropdown });
									Firebase.firestore.notification.updateNewsAsSeen();
								}}
							>
								<div>
									<span id="nav.newsDropdownIcon">
										<img
											id="newsDropdownIconImg"
											className="dropdown-icon"
											width="100%"
											src={notification_icon}
										/>
									</span>
									<NotificationDot
										notificationCount={
											this.props.notifications.news.filter((news) => !news.isSeen).length
										}
									/>
									{this.state.showNotiDropdown ? (
										<div
											id="navnewsDropdownMenu"
											className="nav-dropdown-menu friend-request-dropdown card"
										>
											<div className="card-header">
												<p>Notifications</p>
											</div>
											<div className="card-body p-0">
												{this.props.notifications.news.map((news) => {
													var avatar_path =
														news.sender.avatar_path || avatar[news.sender.gender || 'male'];
													return (
														<div
															className={`row m-0 p-3 pl-4 pr-4 nav-message-single${!news.isSeen
																? ' unseen'
																: ''}`}
															key={uuid()}
															onClick={(event) => {
																event.preventDefault();
																this.props.history.push(news.url);
															}}
														>
															<div className="d-inline mr-3">
																<img
																	width="30"
																	className="round-img"
																	src={avatar_path}
																/>
															</div>
															<div className="d-inline">
																<span>{news.message}</span>
															</div>
														</div>
													);
												})}

												{/* {this.props.session.user.friend_requests.map(request => {
                                                        return (
                                                            <div className="row mb-3" key={`friend_request_${request._id}`}>
                                                                <div className="col-md-2">
                                                                    <img src={request.avatar_path || user_maleImg} width="100%" />
                                                                </div>
                                                                <div className="col-xs-6">
                                                                    <button 
                                                                        className="btn btn-link user-name"
                                                                        onClick={event => {
                                                                            this.props.history.push(`/user/${request._id}`)
                                                                        }}
                                                                    >{request.name}</button>
                                                                </div>
                                                            </div>
                                                        )
                                                    })} */}
											</div>
										</div>
									) : null}
								</div>
							</li>
							<li
								className="nav-dropdown-item"
								id="nav.settingDropdown"
								onClick={() => {
									this.setState({ showSettingDropdown: !this.state.showSettingDropdown });
								}}
							>
								<div>
									<span id="nav.settingDropdownCarret" className="fas fa-caret-down" />
									{this.state.showSettingDropdown ? (
										<div id="nav.settingDropdownMenu" className="nav-dropdown-menu">
											<button
												className="btn"
												onClick={() => {
													this.props.logout();
												}}
											>
												Sign out
											</button>
										</div>
									) : null}
								</div>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		);
	}
}

export default NavBar;
