import {connect} from "react-redux"
import NavBar from "./NavBar"
import userSessionActions from "../../../data/UserSession/actions"
import chatActions from "../../../data/Chat/actions"

const mapStateToProps = (state) => {
    return {
        "session": state.session,
        "notifications": state.notifications
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        logout: () => {
            dispatch(userSessionActions.logoutAction())
        },
        refreshSession: () => {
            dispatch(userSessionActions.sessionUserWillReloadAction())
        },
        createChatbox: ({users, chatName, imagePath}) => {
            dispatch(chatActions.addChatBox({users, chatName, imagePath}))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NavBar)