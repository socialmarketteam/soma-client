import openSocket from "socket.io-client"
const socket = openSocket("http://localhost:3000")
socket.io.timeout(60000)

/**
 * 
 * @param {number} userId 
 * @param {FirestoreEvents} events 
 */
function subscribeToFriendRequest(userId, events) {
    socket.emit('subscribeFriendRequests', userId)
    if(events) {
        if(events.onUpdated) {
            socket.on('friendRequestsUpdated', (docs) => {
                events.onUpdated(docs)
            })
        }
    }
}

/**
 * 
 * @param {any[]} users
 * @param {string} chatName
 */
function createChatChanel(users, chatName = "", callback) {

    socket.on('createChatResponse', (response) => {
        if(callback) {
            callback(response.error, response.doc)
            callback.isCalled = true
        }
    })
    socket.emit('createChatChanel', {users, chatName})
    setTimeout(() => {
        if(!callback.isCalled) callback({error: "TIMEOUT"})
    }, 60000)
}

export {
    subscribeToFriendRequest,
    createChatChanel,
    socket
}