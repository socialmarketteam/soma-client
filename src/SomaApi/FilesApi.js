import config from "./config"
import querystring from "querystring"

var sharedService = null

export default class FilesApi {
    constructor() {
        this.api = config.FILES_API;
        sharedService = this;
    }

    static get shared() {
        return sharedService;
    }

    uploadFile(file, type) {
        const url = `files/${type || `images`}`
        const formData = new FormData();
        formData.append('file', file);

        return this.api.post(url, formData, {
            headers: {
                "content-type": "multipart/form-data"
            }
        })
    }
}

new FilesApi