import config from "./config"
import querystring from "querystring"
import moment from "moment"

let sharedServices = null

class SomaApi {

    static get shared() {
        return sharedServices
    }

    constructor(){
        this.api = config.SERVER_API
        if(!sharedServices) sharedServices = this
    }

    /**
     * Use this method to perform GET requests. Returns an AxiosPromise so
     * remember to handle the promise resolution and rejection using
     * `then((response) => { ... })` and `catch((error) => { ... })`.
     *
     * @param {String} path Path relative to base URL. "/" prefix not needed.
     * @param {Object} query Query parameters
     */
    get(path, query = {}) {
        let urlPath = path

        const urlQuery = querystring.stringify(query)
        urlPath = `${path}?${urlQuery}`

        return this.api.get(urlPath)
    }

    /**
     * Use this method to perform POST requests. Returns an AxiosPromise so
     * remember to handle the promise resolution and rejection using
     * `then((response) => { ... })` and `catch((error) => { ... })`.
     *
     * @param {String} path Path relative to base URL. "/" prefix not needed.
     * @param {Object} payload
     */
    post(path, payload) {
        return this.api.post(path, payload)
    }

    /**
     * Use this method to perform PUT requests. Returns an AxiosPromise so
     * remember to handle the promise resolution and rejection using
     * `then((response) => { ... })` and `catch((error) => { ... })`.
     *
     * @param {String} path Path relative to base URL. "/" prefix not needed.
     * @param {Object} payload
     */
    put(path, payload) {
        return this.api.put(path, payload)
    }

    /**
     * Use this method to perform PATCH requests. Returns an AxiosPromise so
     * remember to handle the promise resolution and rejection using
     * `then((response) => { ... })` and `catch((error) => { ... })`.
     *
     * @param {String} path Path relative to base URL. "/" prefix not needed.
     * @param {Object} payload
     */
    patch(path, payload) {
        return this.api.patch(path, payload)
    }

    /**
     * Use this method to perform DELETE requests. Returns an AxiosPromise so
     * remember to handle the promise resolution and rejection using
     * `then((response) => { ... })` and `catch((error) => { ... })`.
     *
     * @param {String} path Path relative to base URL. "/" prefix not needed.
     * @param {Object} query
     */
    delete(path, query) {
        let queryString = querystring.stringify(query)
        return this.api.delete(`${path}${queryString ? `?${queryString}` : ``}`)
    }

    request() {
        return {
            data: (method, service, collection, query, payload, _id) => {
                let path = `/data/${service}_management_service/${collection}${_id ? `/${_id}` : ``}`;
                switch(method) {
                    case "GET":
                        return this.get(path, query)
                    case "POST":
                        return this.post(path, payload)
                    case "PATCH":
                        return this.patch(path, payload)
                    case "PUT":
                        return this.put(path, payload)
                    case "DELETE":
                        return this.delete(path, payload)
                }
            }
        }
    }

    softDelete(path, item) {
        // Soft delete
        const softDeleteItem = Object.assign({}, { "_id": item._id }, { "deleted_at": moment.utc().format("YYYY-MM-DD HH:mm:ss") })
        const resource = [softDeleteItem]

        return this.patch(path, { resource })
    }

    login(account) {
        return this.api.post("/user/login", account)
    }

    signUp(payload) {
        return this.api.post("/user/register", payload)
    }

    logout() {
        return this.api.post("/user/logout")
    }
}

new SomaApi()
export default SomaApi