import axios from "axios"

const SERVER_API = axios.create({
    "baseURL": API_BASE_URL,
    "timeout": 60000,
    "headers": {
        "X-Application-Key": APPLICATION_KEY
    }
})

const FILES_API = axios.create({
    "baseURL": FILES_API_BASE_URL,
    "timeout": 60000,
    "headers": {
        "X-Application-Key": APPLICATION_KEY
    }
})

const config = {}

export default {
    config,
    SERVER_API,
    FILES_API
}