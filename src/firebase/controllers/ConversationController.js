
class ConversationController {
    constructor(app, collection) {
        this.app = app;
        this.collection = collection;
        this.subscribedConversations = []
    }

    loadConversations(field, condition, value, callback) {
        this.collection.where(field, condition, value).get()
        .then(querySnapshot => {
            var data = [];
            querySnapshot.forEach(doc => {
                data.push(Object.assign({}, doc.data(), {
                    id: doc.id
                }));
            })
            if(callback) callback(null, data)
        })
        .catch(error => {
            if(callback) callback(error)
        })
    }

    sendMessageToConversation(conversationId, message) {
        this.collection.doc(conversationId).get().then(docRef => {
            var data = docRef.data();
            data.messages = data.messages || [];
            data.messages.push(message);
            this.collection.doc(conversationId).set(data).then(() => {
                var firebase = require('../index').default
                var notification = {
                    type: "MSG",
                    data: Object.assign({}, message, {
                        isNotified: false,
                        isSeen: false,
                        conversationId
                    })
                }
                firebase.firestore.notification.pushNotification(notification, data.users);
            }).catch(error => {
                console.log(error)
            })
        })
        .catch(error => {
            console.log(error)
        })
    }

    subscribeToConversation(conversationId, onUpdated) {
        if(!conversationId) return;
        var subscribed = this.subscribedConversations.find(conversation => conversation.id == conversationId);
        if(!subscribed) {
            var unsubscribe = this.collection.doc(conversationId).onSnapshot(doc => {
                let data = doc.data();
                if(onUpdated) onUpdated(Object.assign(data, {id: doc.id}));
            })
            this.subscribedConversations.push({
                id: conversationId,
                unsubscribe
            })
        } else {
            if(subscribed.unsubscribe) subscribed.unsubscribe();
            var unsubscribe = this.collection.doc(conversationId).onSnapshot(doc => {
                let data = doc.data();
                if(onUpdated) onUpdated(Object.assign(data, {id: doc.id}));
            })
            subscribed.unsubscribe = unsubscribe;
        }
    }

    getConversation(conversationId, callback) {
        this.collection.doc(conversationId).get().then(docSnap => {
            if(callback) callback(docSnap)
        }).catch(error => {
            console.log("Cannot get conversation")
        })
    }
}

export default ConversationController