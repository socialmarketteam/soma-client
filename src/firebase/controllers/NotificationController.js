
class NotificationController {
    constructor(app, collection) {
        this.app = app;
        this.collection = collection;
        this.currentUser = "";
    }


    getChannelByUserId(userId) {
        return `notification_user_${userId}`
    }

    updateNewsAsSeen() {
        if(this.currentUser) {
            const currentUserChannel = this.getChannelByUserId(this.currentUser);
            this.getNotificationDoc(currentUserChannel, (error, docSnap) => {
                if(!error) {
                    var data = docSnap.data();
                    data.news = data.news || [];
                    data.news.forEach(news => {
                        news.isSeen = true
                    })
                    docSnap.ref.set(data).catch(error => {})
                }
            })
        }
    }

    updateNewsAsNotified() {
        if(this.currentUser) {
            const currentUserChannel = this.getChannelByUserId(this.currentUser);
            this.getNotificationDoc(currentUserChannel, (error, docSnap) => {
                if(!error) {
                    var data = docSnap.data();
                    data.news = data.news || [];
                    data.news.forEach(news => {
                        news.isNotified = true
                    })
                    docSnap.ref.set(data).catch(error => {})
                }
            })
        }
    }

    updateMessagesAsNotified() {
        if(this.currentUser) {
            const currentUserChannel = this.getChannelByUserId(this.currentUser);
            this.getNotificationDoc(currentUserChannel, (error, docSnap) => {
                if(!error) {
                    var data = docSnap.data();
                    data.messages = data.messages || [];
                    data.messages.forEach(message => {
                        message.isNotified = true
                    })
                    docSnap.ref.set(data).catch(error => {})
                }
            })
        }
    }

    updateMessagesAsSeen(messageId) {
        if(this.currentUser) {
            const currentUserChannel = this.getChannelByUserId(this.currentUser);
            this.getNotificationDoc(currentUserChannel, (error, docSnap) => {
                if(!error) {
                    var data = docSnap.data();
                    data.messages = data.messages || [];
                    var messages = data.messages.filter(msg => msg.conversationId == messageId)
                    if(messages && messages.length) {
                        messages.forEach(message => {
                            message.isSeen = true;
                        })
                    }
                    docSnap.ref.set(data).catch(error => {})
                }
            })
        }
    }

    subscribeUserNotifications(userId, onUpdated) {
        const docId = this.getChannelByUserId(userId);
        if(this.currentUser != userId) {
            this.collection.doc(docId).get().then(docSnap => {
                const subscribe = (ref) => {
                    ref.onSnapshot(doc => {
                        let data = doc.data();
                        if(onUpdated) onUpdated(Object.assign(data, {id: doc.id}))
                    })
                    this.currentUser = userId
                }
                if(!docSnap.exists) {
                    this.createDoc(docId, subscribe)
                } else {
                    subscribe(docSnap.ref)
                }
            })
            .catch(error => {
                console.log(`Cannot get notification doc`)
            })
        }
    }

    /**
     * 
     * @param {{type: string, data: any}} notification 
     * @param {Array} users 
     */
    pushNotification(notification, users) {
        const channels = users.map(userId => {
            return this.getChannelByUserId(userId)
        })
        channels.forEach(channel => {
            try {
                this.pushNotificationToChannel(channel, notification)                
            } catch (error) {
                
            }
        })
    }

    /**
     * 
     * @param {string} channel 
     * @param {{type: string, data:any}} notification 
     */
    pushNotificationToChannel(channel, notification) {
        this.getNotificationDoc(channel, (error,docRef) => {
            if(error) return;
            var data = docRef.data();
            switch(notification.type) {
                case "MSG":
                data.messages = data.messages || [];
                data.messages.push(notification.data);
                break;
                case "NEWS":
                data.news = data.news || [];
                data.news.push(notification.data)
                break;
                default:
                break;
            }
            docRef.ref.set(data).catch(error => {
                // console.log(error)
            });
        })
    }

    getNotificationDoc(channel, callback) {
        this.collection.doc(channel).get().then(docRef => {
            callback(null, docRef)
        })
        .catch(error =>{
            callback(error)
        })
    }

    createDoc(docId, callback) {
        this.collection.doc(docId).set({
            messages: [],
            news: []
        }).then(() => {
            this.collection.doc(docId).get().then(docSnap => {
                if(callback) callback(docSnap.ref)
            }).catch(error => {
                console.log(`Cannot get notification doc`)
            })
        }).catch(error => {
            console.log("Cannot create notification doc")
        })
    }
}

export default NotificationController