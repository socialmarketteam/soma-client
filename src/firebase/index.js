var firebase = require("firebase")
require("firebase/firestore")

import Firestore from "./Firestore"

var config = {
    apiKey: "AIzaSyA10aI_LibTsjM6Tnvm1EM_s9dqtfBkRv8",
    authDomain: "soma-22a67.firebaseapp.com",
    databaseURL: "https://soma-22a67.firebaseio.com",
    projectId: "soma-22a67",
    storageBucket: "soma-22a67.appspot.com",
    messagingSenderId: "461967668309"
};

var app;

class Firebase {
    static init() {
        app = firebase.initializeApp(config)
        this.firestore = new Firestore(app, app.firestore())
    }
}

export default Firebase
