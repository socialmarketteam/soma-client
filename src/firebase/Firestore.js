import ConversationController from "./controllers/ConversationController"
import NotificationController from "./controllers/NotificationController"

class Firestore {
    constructor(app, db) {
        this.app = app;
        this.db = db;
        this.conversation = new ConversationController(app, db.collection("conversations"))
        this.notification = new NotificationController(app, db.collection("notifications"))
    }
}

export default Firestore