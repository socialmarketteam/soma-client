import { createStore, applyMiddleware } from "redux"
import thunk from 'redux-thunk'
import rootReducer from "../data/root/rootReducer"
import SomaApi from "../SomaApi"

var storedState = localStorage.getItem(`soma`);
let persistedReduxState = {}

if(storedState) {
    persistedReduxState = JSON.parse(storedState);

    if(persistedReduxState.session.sessionToken) {
        let api = SomaApi.shared.api
        api.defaults.headers = Object.assign({}, api.defaults.headers, { "Authorization": `Bearer ${persistedReduxState.session.sessionToken}` })
    }
}

const store = createStore(rootReducer, Object.assign({}, persistedReduxState), applyMiddleware(
    thunk
))

store.subscribe(() => {
    let session = store.getState().session || {}
    let api = SomaApi.shared.api
    if(session.sessionToken) {
        api.defaults.headers = Object.assign({}, api.defaults.headers, { "Authorization": `Bearer ${session.sessionToken}` })
        try {
            localStorage.setItem(`soma`, JSON.stringify(store.getState()))
        }
        catch (error) {
            localStorage.clear()
        }
    }
    else {
        api.defaults.headers = Object.assign({}, api.defaults.headers, { "Authorization": `Bearer ${session.sessionToken}` })
        localStorage.clear()
    }
})

export default store