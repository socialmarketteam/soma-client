import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import UserSessionContainer from './containers/views/auth';
import RootView from './containers/views';
import './assets/css/site.css';
import moment from 'moment';

class App extends Component {
	componentDidMount() {
		moment.locale(navigator.language);
	}

	render() {
		return (
			<Router>
				<div>
					<Switch>
						<Route
							path="/auth"
							render={(routerProps) => {
								return <UserSessionContainer {...this.props} {...routerProps} />;
							}}
						/>
						<Route
							exact
							path="*"
							render={(routerProps) => {
								return <RootView {...this.props} {...routerProps} />;
							}}
						/>
					</Switch>
				</div>
			</Router>
		);
	}
}

(function() {
	if (Array.prototype.equals)
		console.warn(
			"Overriding existing Array.prototype.equals. Possible causes: New API defines the method, there's a framework conflict or you've got double inclusions in your code."
		);
	// attach the .equals method to Array's prototype to call it on any array
	Array.prototype.equals = function(array) {
		// if the other array is a falsy value, return
		if (!array) return false;

		// compare lengths - can save a lot of time
		if (this.length != array.length) return false;

		for (var i = 0, l = this.length; i < l; i++) {
			// Check if we have nested arrays
			if (this[i] instanceof Array && array[i] instanceof Array) {
				// recurse into the nested arrays
				if (!this[i].equals(array[i])) return false;
			} else if (this[i] instanceof Object && array[i] instanceof Object) {
				let result;
				Object.keys(this[i]).forEach((key) => {
					if (this[i][key] !== array[i][key]) {
						result = false;
						return;
					}
				});
			} else if (this[i] != array[i]) {
				return false;
			}
		}
		return true;
	};
	// Hide method from for-in loops
	Object.defineProperty(Array.prototype, 'equals', { enumerable: false });
})();

export default App;
